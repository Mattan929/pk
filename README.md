```bash
# php init
# php composer install
# php yii migrate/up --migrationPath=@common/modules/dektrium/yii2-user/migrations
# php yii migrate/up --migrationPath=@yii/rbac/migrations
# php yii migrate --migrationPath=@common/modules/mattan929/yii2-module-event-calendar/src/migrations
# php yii migrate --migrationPath=@vendor/floor12/yii2-module-files/src/migrations/
# php yii migrate/up --migrationPath=@common/modules/pceuropa/yii2-menu/migrations
# php yii migrate
```

Настройка DB в @common/config/main-local.php
