<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%link_on_page}}`.
 */
class m210113_062642_create_link_on_page_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%link_on_page}}', [
            'id' => $this->primaryKey(),
            'title' => $this->string()->notNull(),
            'url' => $this->string(),
            'type' => $this->string(),
            'page_id' => $this->integer(),
            'target_id' => $this->integer(),
            'target_class' => $this->string(),
            'order' => $this->integer()->null(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%link_on_page}}');
    }
}
