<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%settings}}`.
 */
class m210115_121404_create_settings_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%settings}}', [
            'id' => $this->primaryKey(),
            'title' => $this->string()->notNull(),
            'value' => $this->text()->notNull(),
            'additional_value' => $this->text()->notNull(),
            'cant_be_removed' => $this->smallInteger(1)->defaultValue(0),
            'code' => $this->string()
        ]);

        $this->insert('{{%settings}}', [
            'title' => 'Электронная почта для уведомлений',
            'value' => 'pk@kdsu.ru',
            'additional_value' => '',
            'cant_be_removed' => 1,
            'code' => 'notification-e-mail',
        ]);

        $this->insert('{{%settings}}', [
            'title' => 'Футер - Центральный блок',
            'value' => 'Футер - Центральный блок',
            'additional_value' => '',
            'cant_be_removed' => 1,
            'code' => 'footer-center-block',
        ]);

        $this->insert('{{%settings}}', [
            'title' => 'Соц. сети',
            'value' => '{}',
            'additional_value' => '',
            'cant_be_removed' => 1,
            'code' => 'social-networks-block',
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%settings}}');
    }
}

