<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%comment}}`.
 */
class m210115_125852_create_comment_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%comment}}', [
            'id' => $this->primaryKey(),
            'category_id' => $this->integer()->null(),
            'parent_id' => $this->integer()->null(),
            'name' => $this->string(),
            'email' => $this->string(),
            'contact_phone' => $this->string(),
            'body' => $this->text(),
            'status' => $this->integer(1)->defaultValue(0),
            'created_at' => $this->integer(11),
            'updated_at' => $this->integer(11),
            'created_by' => $this->integer(),
            'updated_by' => $this->integer(),
        ]);

        $this->createTable('{{%comment_category}}', [
            'id' => $this->primaryKey(),
            'title' => $this->string(),
            'status' => $this->integer(1)->defaultValue(1),
            'created_at' => $this->integer(11),
            'updated_at' => $this->integer(11),
            'created_by' => $this->integer(),
            'updated_by' => $this->integer(),
        ]);

        $this->addForeignKey('fk-comment-comment_category', 'comment', 'category_id', 'comment_category', 'id', 'cascade', 'cascade');
        $this->addForeignKey('fk-comment-created_by', 'comment', 'created_by', 'user', 'id', 'cascade', 'cascade');
        $this->addForeignKey('fk-comment-updated_by', 'comment', 'updated_by', 'user', 'id', 'cascade', 'cascade');
        $this->addForeignKey('fk-comment_category-created_by', 'comment_category', 'created_by', 'user', 'id', 'cascade', 'cascade');
        $this->addForeignKey('fk-comment_category-updated_by', 'comment_category', 'updated_by', 'user', 'id', 'cascade', 'cascade');

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%comment}}');
    }
}
