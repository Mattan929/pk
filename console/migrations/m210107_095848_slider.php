<?php

use yii\db\Migration;

/**
 * Class m210107_095848_slider
 */
class m210107_095848_slider extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%slider}}', [
            'id' => $this->primaryKey(),
            'title' => $this->text(),
            'url' => $this->text(),
            'order' => $this->integer()->null(),
            'status' => $this->smallInteger(1)->defaultValue(1),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%slider}}');
    }
}
