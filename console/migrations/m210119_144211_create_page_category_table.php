<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%page_category}}`.
 */
class m210119_144211_create_page_category_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%page_category}}', [
            'id' => $this->primaryKey(),
            'title' => $this->string(),
            'status' => $this->integer(1)->defaultValue(1),
            'created_at' => $this->integer(11),
            'updated_at' => $this->integer(11),
            'created_by' => $this->integer(),
            'updated_by' => $this->integer(),
        ]);

        $this->addColumn('{{%page}}', 'category_id', $this->integer()->null());

        $this->addForeignKey('fk-page-page_category', 'page', 'category_id', 'page_category', 'id', 'cascade', 'cascade');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%page}}', 'category_id');
        $this->dropTable('{{%page_category}}');
    }
}
