<?php

namespace console\controllers;

use common\models\AreaInfo;
use Yii;
use yii\console\Controller;

class StatisticController extends Controller
{
    public function actionGetStat()
    {
        if (!file_exists(Yii::getAlias('@frontend') . '/views/statistic/'))
            mkdir(Yii::getAlias('@frontend') . '/views/statistic/');

        $arr = [
            ['f' => 1, 'she' => 0, 'e' => 0, 'mobile' => 1],
            ['f' => 1, 'she' => 0, 'e' => 0, 'mobile' => 0],
            ['f' => 2, 'she' => 0, 'e' => 0, 'mobile' => 0],
            ['f' => 3, 'she' => 0, 'e' => 0, 'mobile' => 0],
            ['f' => 2, 'she' => 0, 'e' => 0, 'mobile' => 1],
            ['f' => 3, 'she' => 0, 'e' => 0, 'mobile' => 1],
        ];

        foreach ($arr as $record){
            $content = AreaInfo::getStat($record);
            $p1 = strpos($content, '<!-- begin -->');
            $p2 = strpos($content, '<!-- end -->');

            $content = mb_strcut($content, $p1, $p2-$p1);
            $content = str_replace('<table', '<table class="table table-bordered"', $content);
            file_put_contents(Yii::getAlias('@frontend') . '/views/statistic/stat-' . implode('-', $record) . '.php', $content);
            echo 'stat-' . implode('-', $record) . '.php - done';
            echo PHP_EOL;
        }


        $arr = [
            ['forwww' => 1, 'f' => 1, 'skillid' => 0, 'e' => 0, 'she' => 0, 'sort' => 0, 'p' => -1, 'mobile' => 1],
            ['forwww' => 1, 'f' => 1, 'skillid' => 68, 'e' => 0, 'she' => 0, 'sort' => 0, 'p' => -1, 'mobile' => 1],
            ['forwww' => 1, 'f' => 1, 'skillid' => 70, 'e' => 0, 'she' => 0, 'sort' => 0, 'p' => -1, 'mobile' => 1],
            ['forwww' => 1, 'f' => 2, 'skillid' => 0, 'e' => 0, 'she' => 0, 'sort' => 0, 'p' => -1, 'mobile' => 1],
            ['forwww' => 1, 'f' => 2, 'skillid' => 68, 'e' => 0, 'she' => 0, 'sort' => 0, 'p' => -1, 'mobile' => 1],
            ['forwww' => 1, 'f' => 2, 'skillid' => 70, 'e' => 0, 'she' => 0, 'sort' => 0, 'p' => -1, 'mobile' => 1],
            ['forwww' => 1, 'f' => 3, 'skillid' => 0, 'e' => 0, 'she' => 0, 'sort' => 0, 'p' => -1, 'mobile' => 1],
            ['forwww' => 1, 'f' => 1, 'skillid' => 0, 'e' => 0, 'she' => 0, 'sort' => 1, 'p' => -1, 'mobile' => 1],
            ['forwww' => 1, 'f' => 2, 'skillid' => 0, 'e' => 0, 'she' => 0, 'sort' => 1, 'p' => -1, 'mobile' => 1],
            ['forwww' => 1, 'f' => 3, 'skillid' => 0, 'e' => 0, 'she' => 0, 'sort' => 1, 'p' => -1, 'mobile' => 1],
            ['forwww' => 1, 'f' => 1, 'skillid' => 0, 'e' => 0, 'she' => 0, 'sort' => 0, 'p' => -1, 'mobile' => 0],
            ['forwww' => 1, 'f' => 1, 'skillid' => 68, 'e' => 0, 'she' => 0, 'sort' => 0, 'p' => -1, 'mobile' => 0],
            ['forwww' => 1, 'f' => 1, 'skillid' => 70, 'e' => 0, 'she' => 0, 'sort' => 0, 'p' => -1, 'mobile' => 0],
            ['forwww' => 1, 'f' => 2, 'skillid' => 0, 'e' => 0, 'she' => 0, 'sort' => 0, 'p' => -1, 'mobile' => 0],
            ['forwww' => 1, 'f' => 2, 'skillid' => 68, 'e' => 0, 'she' => 0, 'sort' => 0, 'p' => -1, 'mobile' => 0],
            ['forwww' => 1, 'f' => 2, 'skillid' => 70, 'e' => 0, 'she' => 0, 'sort' => 0, 'p' => -1, 'mobile' => 0],
            ['forwww' => 1, 'f' => 3, 'skillid' => 0, 'e' => 0, 'she' => 0, 'sort' => 0, 'p' => -1, 'mobile' => 0],
            ['forwww' => 1, 'f' => 1, 'skillid' => 0, 'e' => 0, 'she' => 0, 'sort' => 1, 'p' => -1, 'mobile' => 0],
            ['forwww' => 1, 'f' => 2, 'skillid' => 0, 'e' => 0, 'she' => 0, 'sort' => 1, 'p' => -1, 'mobile' => 0],
            ['forwww' => 1, 'f' => 3, 'skillid' => 0, 'e' => 0, 'she' => 0, 'sort' => 1, 'p' => -1, 'mobile' => 0],
        ];

        foreach ($arr as $record){
            $content = AreaInfo::getSpecmark($record);
            $p1 = strpos($content, '<!-- begin -->');
            $p2 = strpos($content, '<!-- end -->');

            $content = mb_strcut($content, $p1, $p2-$p1);
            $content = str_replace('<table', '<table class="table table-bordered"', $content);
            file_put_contents(Yii::getAlias('@frontend') . '/views/statistic/abits-list-' . implode('-', $record) . '.php', $content);

            echo 'abits-list-' . implode('-', $record) . '.php - done';
            echo PHP_EOL;
        }

        echo '----END----';
        echo PHP_EOL;
        echo PHP_EOL;
    }

}
