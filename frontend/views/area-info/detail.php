<?php

/* @var $this \yii\web\View */
/* @var $areaDetail  */

$this->title = $areaDetail->code . ' ' . $areaDetail->name .(!empty($areaDetail->areaitemname) ? ' (Профиль "' . $areaDetail->areaitemname . '")' : '') ;
$this->params['breadcrumbs'][] = ['label' => 'Направления подготовки и специальности', 'url' => ['/area-info']];
$this->params['breadcrumbs'][] = ['label' => $this->title ];
?>

<div class="page">
    <p>
        <?= nl2br($areaDetail->description) ?>
    </p>
</div>
