<?php
/* @var $this yii\web\View */
/** @var \common\models\AreaInfo $model */
/** @var array $subjects */

use yii\helpers\Html;

$this->title = 'Направления подготовки и специальности';
$this->params['breadcrumbs'][] = ['label' => $this->title];

?>
<div class="page">
    <div class="row">
        <?php $form = \yii\bootstrap\ActiveForm::begin([
            'method' => 'POST'
        ]); ?>
        <div class="col-md-4">
            <?= $form->field($model, 'form')->dropDownList($model->getFormNames(), ['prompt' => 'Выберите форму обучения']); ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'pay')->dropDownList($model->getPayNames()); ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'skillid')->dropDownList($model->getSkillNames(), ['prompt' => 'Выберите уровень подготовки']); ?>
        </div>
        <div class="col-md-12">
            <?php
            $icon = '<i class="glyphicon glyphicon-info-sign" data-toogle="tooltip" title="Укажите предметы, по которым Вы сдали (планируете сдавать) ЕГЭ, и в списке останутся только те направления подготовки, на которые Вы сможете подать заявление с указанным набором экзаменов"></i>';
            $template = '{label} ' . $icon . '{beginWrapper}{input}{hint}{error}{endWrapper}';
            ?>
            <?= $form->field($model, 'subjs', ['template' => $template])->checkboxList(\yii\helpers\ArrayHelper::map($subjects, 'id', 'name'),[
                'item'=>function ($index, $label, $name, $checked, $value) {
                    return '<div class="col-md-3">'. Html::checkbox($name,$checked,['label'=>$label,'value'=>$value]).'</div>';
                }
            ]); ?>
        </div>
        <div class="col-md-12">
            <?= Html::submitButton('Применить фильтр', ['class' => 'btn btn-primary']) ?>
        </div>

        <?php \yii\bootstrap\ActiveForm::end(); ?>
    </div>

    <div class="row">
        <div class="col-md-12">
            <table width="100%">
                <?php
                $cur_skill = 0;
                $skills = $model->getSkillNames();
                foreach ($areaDetails as $area) {
                    if ($area->skillid != $cur_skill) {
                        echo "<tr><td class='skill'><h2>{$skills[$area->skillid]}</h2></td></tr>";
                        $cur_skill = $area->skillid;
                    }
                    echo "
	<tr>
		<td>
			<a href='/area-info/details?areaid={$area->areaid}&areaitemid={$area->areaitemid}' target='_blank'>{$area->code} {$area->name}</a>
		</td>
	</tr>
	";
                }
                ?>
            </table>
        </div>
    </div>
</div>

