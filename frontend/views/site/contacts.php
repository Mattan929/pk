<?php
/* @var $this \yii\web\View */

$this->title = 'Контактная информация';
$this->params['breadcrumbs'][] = ['label' => $this->title];
?>

<div class="page contacts-page">
    <div class="category-desc">
        <p><strong>Прием документов осуществляется по
                адресу:</strong></p>
        <p>г. Курган, ул. Советская, 63 (строение 4), холл (первый
            этаж), ауд. 308</p>
        <p><strong>Время работы:</strong></p>
        <p>Понедельник - пятница с 9:00 до 17:00;</p>
        <p>Суббота,&nbsp;воскресенье - выходной</p>
        <p><strong>Телефоны:</strong></p>
        <p>8 (3522) 65-30-30 (многоканальный) (время работы с 9:00 до 17:00)</p>
        <p><strong>Электронная почта:</strong>
            <a href="mailto:pk@kgsu.ru">pk@kgsu.ru</a>
        </p>

        <table style="width: 633px;" border="1" cellspacing="0" cellpadding="0">
            <tbody>
            <tr>
                <td valign="top" width="311">
                    <p>Ответственный секретарь приёмной комиссии</p>
                </td>
                <td valign="top" width="142">
                    <p>&nbsp;Киселева Мария Михайловна</p>
                </td>
            </tr>
            <tr>
                <td valign="top" width="311">
                    <p>Заместитель ответственного секретаря по оформлению документации</p>
                </td>
                <td valign="top" width="142">
                    <p>&nbsp;Жмакина Юлия Рудольфовна</p>
                </td>
            </tr>
            <tr>
                <td valign="top" width="311">
                    <p>Заместитель ответственного секретаря по проведению вступительных испытаний (по очной форме
                        обучения)</p>
                </td>
                <td valign="top" width="142">&nbsp;Говоркова Людмила Ивановна</td>
            </tr>
            <tr>
                <td valign="top" width="311">
                    <p>Заместитель ответственного секретаря по проведению вступительных испытаний (по заочной форме
                        обучения)</p>
                </td>
                <td valign="top" width="142">
                    <p>&nbsp;Мухина Елена Александровна</p>
                </td>
            </tr>
            <tr>
                <td width="311">
                    <p>Заместитель ответственного секретаря по приему в магистратуру</p>
                </td>
                <td width="142">
                    <p>&nbsp;Милюкова Елена Владимировна</p>
                </td>
            </tr>
            </tbody>
        </table>
    </div>

</div>
