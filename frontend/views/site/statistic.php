<?php

/* @var $this \yii\web\View */

/* @var $content false|string */

use yii\bootstrap\Nav;

$this->params['breadcrumbs'][] = ['label' => $this->title];

?>

<div class="page">
    <div class="row">
        <div class="col-lg-9">
            <?= $content ?>
        </div>
        <div class="col-lg-3 fixed-block">
            <div class="menu">
                <?= Nav::widget([
                    'options' => ['class' => 'nav-pills nav-stacked nav-stat'],
                    'activateParents' => true,
                    'items' => [
                        [
                            'label' => 'СТАТИСТИКА ЗАЯВЛЕНИЙ',
                            'items' => [
                                ['label' => 'Очная', 'url' => ['site/statistics', 'form' => 1]],
                                ['label' => 'Заочная', 'url' => ['site/statistics', 'form' => 2]],
                                ['label' => 'Очно-заочная', 'url' => ['site/statistics', 'form' => 3]],
                            ]
                        ],
                        [
                            'label' => 'СПИСКИ АБИТУРИЕНТОВ С БАЛЛАМИ И ВЫБРАННЫМИ НАПРАВЛЕНИЯМИ ПОДГОТОВКИ',
                            'items' => [
                                ['label' => 'Очная форма обучения', 'url' => ['site/abits-list', 'form' => 1, 'skillid' => 0, 'sort' => 0]],
                                ['label' => 'Очная форма обучения. Магистратура', 'url' => ['site/abits-list', 'form' => 1, 'skillid' => 68]],
                                ['label' => 'Очная форма обучения. Аспирантура', 'url' => ['site/abits-list', 'form' => 1, 'skillid' => 70]],
                                ['label' => 'Заочная форма обучения', 'url' => ['site/abits-list', 'form' => 2, 'skillid' => 0, 'sort' => 0]],
                                ['label' => 'Заочная форма обучения. Магистратура', 'url' => ['site/abits-list', 'form' => 2, 'skillid' => 68]],
                                ['label' => 'Заочная форма обучения. Аспирантура', 'url' => ['site/abits-list', 'form' => 2, 'skillid' => 70]],
                                ['label' => 'Очно-заочная форма обучения', 'url' => ['site/abits-list', 'form' => 3, 'skillid' => 0, 'ext' => 0, 'sort' => 0]],
                            ]
                        ],
                        [
                            'label' => 'СПИСКИ АБИТУРИЕНТОВ С БАЛЛАМИ И ВЫБРАННЫМИ НАПРАВЛЕНИЯМИ ПОДГОТОВКИ, УПОРЯДОЧЕННЫЕ ПО ФАКТУ ПРЕДОСТАВЛЕНИЯ ОРИГИНАЛА ДОКУМЕНТА ОБ ОБРАЗОВАНИИ, КАТЕГОРИИ ПОСТУПЛЕНИЯ И КОНКУРСНЫМ БАЛЛАМ',
                            'items' => [
                                ['label' => 'Очная форма обучения', 'url' => ['site/abits-list', 'form' => 1, 'skillid' => 0, 'sort' => 1]],
                                ['label' => 'Заочная форма обучения', 'url' => ['site/abits-list', 'form' => 2, 'skillid' => 0, 'sort' => 1]],
                                ['label' => 'Очно-заочная форма обучения', 'url' => ['site/abits-list', 'form' => 3, 'skillid' => 0, 'ext' => 0, 'sort' => 1]],
                            ]
                        ],
                        ['label' => 'В начало', 'url' => '#'],
                    ],
                ]);
                ?>
            </div>
        </div>
    </div>
</div>
