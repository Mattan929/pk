<?php

use common\models\Settings;
use common\widgets\ContactFormWidget\ContactFormWidget;
use kv4nt\owlcarousel\OwlCarouselWidget;
use mattan929\calendar\widgets\EventCalendarWidget\EventCalendarWidget;

$this->title = 'Курганский государственный университет - Приемная комиссия';

?>

<div class="row">
    <div class="slider">
        <?php
        OwlCarouselWidget::begin([
            'container' => 'div',
            'assetType' => OwlCarouselWidget::ASSET_TYPE_CDN,
            'containerOptions' => [
                'id' => 'slider',
                'class' => 'container-class owl-theme',
            ],
            'pluginOptions' => [
                'autoplay' => true,
                'autoplayTimeout' => 10000,
                'items' => 1,
                'margin' => 10,
                'nav' => true,
                'dots' => false,
                'center' => true,
                'merge' => true,
                'loop' => true,
                'navContainer' => '#slider',
                'navElement' => 'div',
                'navClass' => [
                    'slider-prev',
                    'slider-next'
                ],
                'navContainerClass' => 'owl-nav slider-nav',
                'navText' => [
                    '<i class="glyphicon glyphicon-menu-left"></i>',
                    '<i class="glyphicon glyphicon-menu-right"></i>'
                ],
                'itemsDesktop' => [1199, 1],
                'itemsDesktopSmall' => [979, 1],
                'responsive' => [
                    0 => [
                        'items' => 1,
                        'nav' => false,
                    ],
                    768 => [
                        'items' => 1,
                        'nav' => true,
                    ],
                ],
            ]
        ]);
        ?>
        <?php /** @var \common\models\Slider[] $sliders */
        foreach ($sliders as $slider): ?>
            <div class="item">
                <a href="<?= !empty($slider->url) ? $slider->url : '#' ?>">
                    <div class="slide"
                         style="height: 400px; background-image: url('<?= $slider->image ?>'); background-size: cover"></div>
                    <div class="slide-mobile"
                         style="height: 400px; background-image: url('<?= $slider->image_mobile ?>'); background-size: cover"></div>
                </a>
            </div>
        <?php endforeach; ?>
        <?php OwlCarouselWidget::end(); ?>
    </div>
</div>
<div class="">
    <h3 class="text-center">ГЛАВНЫЕ НОВОСТИ</h3>
    <div class="main-news">
        <?php
        OwlCarouselWidget::begin([
            'container' => 'div',
            'assetType' => OwlCarouselWidget::ASSET_TYPE_CDN,
            'containerOptions' => [
                'id' => 'main-news',
                'class' => 'container-class owl-theme',
            ],
            'pluginOptions' => [
                'autoplay' => true,
                'autoplayTimeout' => 10000,
                'items' => 1,
                'margin' => 20,
                'nav' => true,
                'dots' => false,
                'center' => true,
                'merge' => true,
                'loop' => true,
                'navContainer' => '#main-news',
                'navElement' => 'div',
                'navClass' => [
                    'slider-prev',
                    'slider-next'
                ],
                'navContainerClass' => 'owl-nav slider-nav',
                'navText' => [
                    '<i class="glyphicon glyphicon-menu-left"></i>',
                    '<i class="glyphicon glyphicon-menu-right"></i>'
                ],
                'itemsDesktop' => [1199, 1],
                'itemsDesktopSmall' => [979, 1],
                'responsive' => [
                    0 => [
                        'items' => 1,
                        'nav' => false,
                    ],
                    768 => [
                        'items' => 1,
                        'nav' => true,
                    ],
                ],
            ]
        ]);
        ?>

        <?= \common\widgets\NewsWidgets\NewsList::widget(['mode' => 'simple']) ?>

        <?php OwlCarouselWidget::end(); ?>
    </div>
    <a href="<?= \yii\helpers\Url::to(['site/news']) ?>"><u><h4 class="text-center">БОЛЬШЕ НОВОСТЕЙ</h4></u></a>

</div>
<div class="row">
    <h3 class="text-center">ПРИЁМ <?= date('Y')?></h3>
    <div class="main-page-block">
        <?php
        /** @var \common\models\Page[] $pages */
        foreach ($pages as $key => $page): ?>
            <?= $key%3 === 0 ? '<div class="clearfix"></div>' : '' ?>
            <div class="col-md-4">
                <div class="panel">
                    <div class="panel-header">
                        <a href="<?= \yii\helpers\Url::to(['site/page', 'id' => $page->id]) ?>">
                            <div class="flag-title">
                                <?= $page->title ?>
                            </div>
                            <div class="img-block">
                                <img src="<?= $page->image->getPreviewWebPath(396, 0, true) ?>" alt="">
                            </div>
                        </a>
                    </div>
                    <div class="panel-body">
                        <div class="text">
                            <ul class="list-unstyled">
                                <?php foreach ($page->links as $link):
                                    /** @var \common\models\LinkOnPage $link */?>
                                <li>
                                    <a href="<?= $link->urlToTarget?>" target="<?= $link->type ?>"><?= $link->title?></a>
                                </li>
                                <?php endforeach; ?>

                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        <?php endforeach; ?>
    </div>
</div>

<?= EventCalendarWidget::widget() ?>

<div class="row">
    <h3 class="text-center">ЗАДАЙТЕ ВОПРОС</h3>
    <div class="feedback-form" id="feedback-form">
        <?= ContactFormWidget::widget(['send_to' => Settings::getValue('notification-e-mail'), 'subject' => 'Форма обратной связи']) ?>
    </div>
</div>