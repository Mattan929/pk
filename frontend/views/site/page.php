<?php

/* @var $this \yii\web\View */
/* @var $model \common\models\Page|null */
$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => \yii\helpers\StringHelper::truncate($this->title , 30) ];

?>
<div class="page">
    <div class="row">
        <div class="col-lg-9">
            <?= $model->content ?>
        </div>
        <div class="col-lg-3">
            <div class="date-block">
                <p>
                    <em>
                        Опубликовано:<br>
                        <?= Yii::$app->formatter->asDate($model->updated_at, 'dd MMMM Y hh:mm') ?>
                    </em>
                </p>
            </div>
            <?php if ($model->image !== null): ?>
            <div class="img-block">
                <a class="fancybox" data-fancybox="post-image" rel="group" href="<?= $model->image ?>">
                    <img src="<?= $model->image->getPreviewWebPath(282, 0, true) ?>"
                         alt="<?= $model->title ?>">
                </a>
            </div>
            <?php endif; ?>
            <div class="menu">
                <ul class="list-unstyled">
                    <?php foreach ($model->links as $link):
                        /** @var \common\models\LinkOnPage $link */?>
                        <li>
                            <a href="<?= $link->urlToTarget?>" target="<?= $link->type ?>"><?= $link->title?></a>
                        </li>
                    <?php endforeach; ?>

                </ul>
            </div>
        </div>
    </div>
</div>
