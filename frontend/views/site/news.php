<?php

/* @var $this \yii\web\View */
/* @var $posts \yii\db\ActiveQuery */

use common\widgets\NewsWidgets\NewsList;

$this->title = 'Новости';
$this->params['breadcrumbs'][] = ['label' => $this->title];

?>

<div class="news-page">

    <?= NewsList::widget(['news' => $posts, 'items' => 20, 'showmore' => true, 'withoutCategory' => true]) ?>
</div>

