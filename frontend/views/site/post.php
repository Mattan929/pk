<?php

/* @var $this \yii\web\View */
/* @var $post \common\models\Post|null */

$this->title = $post->title;
$this->params['breadcrumbs'][] = ['label' => 'Новости', 'url' => ['/site/news']];
$this->params['breadcrumbs'][] = ['label' => \yii\helpers\StringHelper::truncate($this->title , 30) ];

?>
<div class=" page post-page">
    <div class="row">
        <div class="col-lg-9">
            <?= $post->content ?>
        </div>
        <div class="col-lg-3">
            <div class="date-block">
                <p>
                    <em>
                        Опубликовано:<br>
                        <?= Yii::$app->formatter->asDate($post->publish_date, 'dd MMMM Y hh:mm') ?>
                    </em>
                </p>
            </div>
            <div class="img-block">
                <a class="fancybox" data-fancybox="post-image" rel="group" href="<?= $post->post_image ?>">
                    <img src="<?= $post->post_image->getPreviewWebPath(282, 0, true) ?>"
                         alt="<?= $post->title ?>">
                </a>
            </div>
        </div>
    </div>
</div>
