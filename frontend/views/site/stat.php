<?php

/* @var $this \yii\web\View */
/* @var $data array */

use yii\bootstrap\Nav;

$this->title = 'Статистика и списки поступающих';
$this->params['breadcrumbs'][] = ['label' => $this->title];

?>

<div class="page">
    <STYLE TYPE="text/css">
        tr.R0 {
            height: 17px;
        }

        tr.R0 td.R0C0 {
            font-family: Times New Roman;
            font-size: 10pt;
            font-style: normal;
            font-weight: bold;
            text-align: center;
        }

        tr.R0 td.R2C0 {
            font-family: Times New Roman;
            font-size: 10pt;
            font-style: normal;
            font-weight: bold;
            text-align: left;
        }

        tr.R1 {
            height: 15px;
        }

        tr.R1 td.R1C0 {
            font-family: Times New Roman;
            font-size: 10pt;
            font-style: normal;
            text-align: center;
            vertical-align: middle;
        }

        tr.R14 {
            height: 16px;
        }

        tr.R14 td.R14C0 {
            font-family: Times New Roman;
            font-size: 9pt;
            font-style: normal;
            font-weight: bold;
            text-align: left;
            vertical-align: middle;
            padding-left: 16px;
        }

        tr.R16 {
            height: 65px;
        }

        tr.R16 td.R16C0 {
            font-family: Times New Roman;
            font-size: 10pt;
            font-style: normal;
            text-align: center;
            vertical-align: middle;
            border-left: #000000 1px solid;
            border-top: #000000 1px solid;
            border-bottom: #000000 1px solid;
            border-right: #000000 1px solid;
        }

        tr.R16 td.R16C2 {
            font-family: Times New Roman;
            font-size: 10pt;
            font-style: normal;
            text-align: center;
            vertical-align: middle;
            overflow: visible;
            border-left: #000000 1px solid;
            border-top: #000000 1px solid;
            border-bottom: #000000 1px solid;
            border-right: #000000 1px solid;
        }

        tr.R17 {
            height: 33px;
        }

        tr.R17 td.R17C0 {
            font-family: Microsoft Sans Serif;
            font-size: 10pt;
            font-style: normal;
            color: #a0522d;
            text-align: center;
            vertical-align: middle;
            border-left: #000000 1px solid;
            border-top: #000000 1px solid;
            border-bottom: #000000 1px solid;
            border-right: #000000 1px solid;
        }

        tr.R17 td.R17C7 {
            font-family: Microsoft Sans Serif;
            font-size: 10pt;
            font-style: normal;
            color: #a0522d;
            text-align: left;
            vertical-align: middle;
            border-left: #000000 1px solid;
            border-top: #000000 1px solid;
            border-bottom: #000000 1px solid;
            border-right: #000000 1px solid;
        }

        table {
            table-layout: fixed;
            padding: 0px;
            padding-left: 2px;
            vertical-align: bottom;
            border-collapse: collapse;
            width: 100%;
            font-family: Arial;
            font-size: 8pt;
            font-style: normal;
        }

        td {
            padding: 0px;
            padding-left: 2px;
            overflow: hidden;
        }

        .panel-collapse {
            overflow: auto;
        }
    </STYLE>
    <div class="row">
        <div class="col-lg-9">
            <?php
            if (!is_array($data)) {
                echo $data;
            } else { ?>
                <?php foreach ($data as $key => $report) { ?>
                    <div class="panel-group" id="accordion">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <a data-toggle="collapse" data-parent="#accordion" href="#collapse<?= $key ?>">
                                    <h4 class="panel-title">
                                        <?= $report['name'] ?>
                                    </h4>
                                </a>
                            </div>
                            <div id="collapse<?= $key ?>" class="panel-collapse collapse">
                                <div class="panel-body">
                                    <?= $report['content'] ?>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php } ?>
            <?php } ?>
        </div>
        <div class="col-lg-3 fixed-block">
            <div class="menu">
                <?= Nav::widget([
                    'options' => ['class' => 'nav-pills nav-stacked nav-stat'],
                    'activateParents' => true,
                    'items' => [
                        [
                            'label' => 'СТАТИСТИКА ЗАЯВЛЕНИЙ',
                            'items' => [
                                ['label' => 'Очная', 'url' => ['site/statistic', 'form' => 'ochnoe']],
                                ['label' => 'Заочная', 'url' => ['site/statistic', 'form' => 'zaochnoe']],
                                ['label' => 'Очно-заочная', 'url' => ['site/statistic', 'form' => 'ochno-zaochnoe']],
                            ]
                        ],
//                        [
//                            'label' => 'СПИСКИ АБИТУРИЕНТОВ С БАЛЛАМИ И ВЫБРАННЫМИ НАПРАВЛЕНИЯМИ ПОДГОТОВКИ',
//                            'items' => [
//                                ['label' => 'Очная форма обучения', 'url' => ['site/abits-list', 'form' => 1, 'skillid' => 0, 'sort' => 0]],
//                                ['label' => 'Очная форма обучения. Магистратура', 'url' => ['site/abits-list', 'form' => 1, 'skillid' => 68]],
//                                ['label' => 'Очная форма обучения. Аспирантура', 'url' => ['site/abits-list', 'form' => 1, 'skillid' => 70]],
//                                ['label' => 'Заочная форма обучения', 'url' => ['site/abits-list', 'form' => 2, 'skillid' => 0, 'sort' => 0]],
//                                ['label' => 'Заочная форма обучения. Магистратура', 'url' => ['site/abits-list', 'form' => 2, 'skillid' => 68]],
//                                ['label' => 'Заочная форма обучения. Аспирантура', 'url' => ['site/abits-list', 'form' => 2, 'skillid' => 70]],
//                                ['label' => 'Очно-заочная форма обучения', 'url' => ['site/abits-list', 'form' => 3, 'skillid' => 0, 'ext' => 0, 'sort' => 0]],
//                            ]
//                        ],
//                        [
//                            'label' => 'СПИСКИ АБИТУРИЕНТОВ С БАЛЛАМИ И ВЫБРАННЫМИ НАПРАВЛЕНИЯМИ ПОДГОТОВКИ, УПОРЯДОЧЕННЫЕ ПО ФАКТУ ПРЕДОСТАВЛЕНИЯ ОРИГИНАЛА ДОКУМЕНТА ОБ ОБРАЗОВАНИИ, КАТЕГОРИИ ПОСТУПЛЕНИЯ И КОНКУРСНЫМ БАЛЛАМ',
//                            'items' => [
//                                ['label' => 'Очная форма обучения', 'url' => ['site/abits-list', 'form' => 1, 'skillid' => 0, 'sort' => 1]],
//                                ['label' => 'Заочная форма обучения', 'url' => ['site/abits-list', 'form' => 2, 'skillid' => 0, 'sort' => 1]],
//                                ['label' => 'Очно-заочная форма обучения', 'url' => ['site/abits-list', 'form' => 3, 'skillid' => 0, 'ext' => 0, 'sort' => 1]],
//                            ]
//                        ],
                        ['label' => 'В начало', 'url' => '#'],
                    ],
                ]);
                ?>
            </div>
        </div>
    </div>
</div>

