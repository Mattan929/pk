<?php

/* @var $this \yii\web\View */

/* @var $content string */

use common\models\Settings;
use pceuropa\menu\Menu;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\helpers\Url;
use yii\widgets\Breadcrumbs;
use frontend\assets\AppAsset;
use common\widgets\Alert;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php $this->registerCsrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<div class="wrap">
    <?php
    $staticItemMain = [
//        ['label' => 'Направления подготовки', 'url' => ['/area-info']],
    ];
    $logo = Html::img(\yii\helpers\Url::to(['/images/logo.png']), ['height' => 50]);
    NavBar::begin([
        'brandLabel' => $logo,
        'brandUrl' => Yii::$app->homeUrl,
        'options' => [
            'class' => 'navbar-pk navbar-fixed-top',
        ],
    ]);

    echo Nav::widget([
        'options' => ['class' => 'navbar-nav navbar-left'],
        'items' => ArrayHelper::merge($staticItemMain, Menu::NavbarLeft('Главное меню')),
    ]);
    $menuItems = [];
//    if (Yii::$app->user->isGuest) {
//        $menuItems[] = ['label' => 'Вход', 'url' => ['/user/security/login']];
//    } else {
//        $menuItems[] = '<li>'
//            . Html::beginForm(['/user/security/logout'], 'post')
//            . Html::submitButton(
//                'Выйти (' . Yii::$app->user->identity->username . ')',
//                ['class' => 'btn btn-link logout']
//            )
//            . Html::endForm()
//            . '</li>';
//    }
    echo Nav::widget([
        'options' => ['class' => 'navbar-nav navbar-right'],
        'items' => ArrayHelper::merge(Menu::NavbarRight('Главное меню'), $menuItems),
    ]);
    NavBar::end();
    ?>
    <div class="container">
        <div class="row">
            <?php
            //Второе меню
            $staticItem = [
                ['label' => 'Направления подготовки', 'url' => ['/area-info']],
                ['label' => 'Статистика и списки поступающих', 'url' => ['/statistics', 'form' => 1]]
            ];
            echo Nav::widget([
                'options' => ['class' => 'navbar-pk-second navbar-nav navbar-left'],
                'items' => ArrayHelper::merge($staticItem, Menu::NavbarLeft('Второе меню')),
            ]);

            echo Nav::widget([
                'options' => ['class' => 'navbar-pk-second navbar-nav navbar-right'],
                'items' => Menu::NavbarRight('Второе меню'),
            ]);
            ?>
        </div>
        <?php if (Yii::$app->request->url !== '/'): ?>
            <h1><?= Html::encode($this->title) ?></h1>
        <?php endif; ?>
        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
        <?= Alert::widget() ?>
        <?= $content ?>
    </div>
</div>

<footer class="footer">
    <div class="container">
        <div class="row">
            <div class="col-md-2">
                <?= Nav::widget([
                    'options' => ['class' => 'nav-pills nav-stacked nav-pk-footer'],
                    'items' => ArrayHelper::merge(Menu::NavbarLeft(1), Menu::NavbarRight(1)),
                ]);
                ?>
            </div>
            <div class="col-md-3">
                <?= Nav::widget([
                    'options' => ['class' => 'nav-pills nav-stacked nav-pk-footer'],
                    'items' => ArrayHelper::merge($staticItem, Menu::NavbarLeft(2), Menu::NavbarRight(2)),
                ]);
                ?>
            </div>
            <div class="col-md-4">
                <?= Settings::getValue('footer-center-block') ?>
            </div>
            <div class="col-md-3">
                <?php foreach (Settings::getValue('social-networks-block') as $social): ?>
                    <div class="col-md-2 col-sm-3 col-xs-3 text-center">
                        <a class="social-icon <?= $social->name ?>" href="<?= Url::to($social->link) ?>"
                           target="_blank">
                        </a>
                    </div>
                <?php endforeach; ?>
            </div>
        </div>
        <p class="pull-left">&copy; <?= Html::encode(Yii::$app->name) ?> <?= date('Y') ?></p>
        <span class="pull-right" style="color: #1665A8">developed by @mattan929</span>
    </div>
</footer>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
