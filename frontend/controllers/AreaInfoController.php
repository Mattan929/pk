<?php

namespace frontend\controllers;

use common\models\AreaInfo;
use yii\helpers\ArrayHelper;

class AreaInfoController extends \yii\web\Controller
{

    public function actionIndex()
    {
        $model = new AreaInfo();
        $param = \Yii::$app->request->post();
        $areaDetails = $model->search($param);

        $subjects = $model->getEgeSubjects();

        return $this->render('index', ['model' => $model,'subjects' => $subjects, 'areaDetails' => $areaDetails]);
    }

    public function actionDetails($areaid = null, $areaitemid = null)
    {
        $model = new AreaInfo();

        $areaDetail = $model->getDetails($areaid, $areaitemid);
//        var_dump($areaDetail);
//        die;
        return $this->render('detail', ['areaDetail' => $areaDetail]);
    }

}
