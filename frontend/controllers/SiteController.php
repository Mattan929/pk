<?php
namespace frontend\controllers;

use common\models\AreaInfo;
use common\models\ContactForm;
use common\models\Page;
use common\models\Post;
use common\models\Slider;
use frontend\models\ResendVerificationEmailForm;
use frontend\models\VerifyEmailForm;
use Yii;
use yii\base\Exception;
use yii\base\InvalidArgumentException;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\models\LoginForm;
use frontend\models\PasswordResetRequestForm;
use frontend\models\ResetPasswordForm;
use frontend\models\SignupForm;
use yii\web\NotFoundHttpException;

/**
 * Site controller
 */
class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout', 'signup'],
                'rules' => [
                    [
                        'actions' => ['signup'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return mixed
     */
    public function actionIndex()
    {
        $sliders = Slider::find()->where(['status' => Slider::STATUS_ACTIVE])->orderBy('order')->all();
        $pages = Page::find()->where(['status' => Page::STATUS_PUBLISH, 'on_main_page' => 1])->all();

        return $this->render('index', ['sliders' => $sliders, 'pages' => $pages]);
    }

    /**
     * Displays contact page.
     *
     * @return mixed
     */
    public function actionContact($send_to)
    {
        $data = Yii::$app->request->post('ContactForm', false);
        if ($data) {
            $model = new ContactForm();
            $model->scenario = $data['scenario'];
            if ($model->load(Yii::$app->request->post()) && $model->contact($send_to)) {
                switch ($model->scenario) {
                    case ContactForm::SCENARIO_CONTACT:
                        $message = 'Ваше сообщение отправлено.';
                        break;
                    default: $message = 'Ваше действие имело успех.';

                }
                Yii::$app->session->setFlash('success', $message);

                return $this->redirect(Yii::$app->request->referrer);
            }
        }

        Yii::$app->session->setFlash('danger', 'Что то пошло не так... Повторите попытку позже.');

        return $this->redirect(Yii::$app->request->referrer);
    }

    /**
     * Displays about page.
     *
     * @return mixed
     */
    public function actionAbout()
    {
        return $this->render('about');
    }

    /**
     * Displays Contacts page.
     *
     * @return mixed
     */
    public function actionContacts()
    {
        if (($model = Page::findOne(['slug' => 'kontakty'])) !== null) {
            return $this->render('page', ['model' => $model]);
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     * Displays post page.
     *
     * @return mixed
     */
    public function actionPost($id)
    {
        $post = $this->findPost($id);
        return $this->render('post', ['post' => $post]);
    }


    /**
     * Displays Pages page.
     *
     * @return mixed
     */
    public function actionPage($id)
    {
        $model = $this->findPage($id);

        return $this->render('page', ['model' => $model]);
    }

    /**
     * Displays news page.
     *
     * @return mixed
     */
    public function actionNews()
    {
        $posts = Post::findAll(['status' => Post::STATUS_PUBLISH]);
        return $this->render('news', ['posts' => $posts]);
    }

    protected function findPost($id)
    {
        if (($model = Post::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    protected function findPage($id)
    {
        if (($model = Page::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionStatistics($form = 1)
    {
        $this->view->title = 'Статистика и списки поступающих';
        $isMobile = (integer)\Yii::$app->devicedetect->isMobile();
        $fileName = 'stat-'.$form . '-' . '0' . '-' . '0' . '-' . $isMobile;

        if (!file_exists(Yii::getAlias("@frontend")."/views/statistic/{$fileName}.php"))
            throw new NotFoundHttpException('The requested page does not exist.');

        if($isMobile){
            $this->view->registerJs("
            jQuery(document).ready(function () {
                jQuery('.spec-link').click(function (eo) {
                    jQuery(eo.target).next().slideToggle('fast');
                });
                jQuery('.minimize-descr').click(function (eo) {
                    jQuery(eo.target).parent().slideToggle('fast');
                });
                
            });
            ");
        }

        $content = $this->renderPartial('/statistic/' . $fileName);
        return $this->render('statistic', ['content' => $content]);
    }

    public function actionAbitsList($form = 1, $skillid = 0, $ext = 0, $she = 0, $sort = 0)
    {
        $this->view->title = 'Статистика и списки поступающих';
        $isMobile = (integer)\Yii::$app->devicedetect->isMobile();

        $fileName = "abits-list-1-{$form}-{$skillid}-{$ext}-{$she}-{$sort}--1-{$isMobile}";

        if (!file_exists(Yii::getAlias('@frontend') . "/views/statistic/{$fileName}.php"))
            throw new NotFoundHttpException('Не удалось найти сведений, зайдите позже');

        if($isMobile){
            $this->view->registerJs("
            jQuery(document).ready(function () {
                jQuery('.spec-link').click(function (eo) {
                    jQuery(eo.target).next().slideToggle('fast');
                });
                jQuery('.minimize-descr').click(function (eo) {
                    jQuery(eo.target).parent().slideToggle('fast');
                });
                
            });
            ");
        }

        $content = $this->renderPartial("/statistic/{$fileName}");
        return $this->render('statistic', ['content' => $content]);
    }

    public function actionZAbitsList($form = 1, $skillid = 0, $ext = 0, $she = 0, $sort = 0)
    {
        $this->view->title = 'Статистика и списки поступающих';
        $isMobile = (integer)\Yii::$app->devicedetect->isMobile();

        $params = [
            'f' => 1,
            'p' => 0,
            'rang' => 0,
            'withouthvost' => 1,
            'prog' => 0,
            'type' => 0,
            'att' => 0, // аттестат
            'she' => 0, //второе высшее
            'e' => 0, //сокращенка
            'withoutdeadline' => 0,
            'mobile' => $isMobile
        ];

//        if (!file_exists(Yii::getAlias('@frontend') . "/views/statistic/abits-list-{$form}-{$skillid}-{$ext}-{$she}-{$sort}"))
//            throw new NotFoundHttpException('The requested page does not exist.');

        $content = AreaInfo::getZList($params);

        $p1 = strpos($content, '<!-- begin -->');
        $p2 = strpos($content, '<!-- end -->');

        $content = mb_strcut($content, $p1, $p2-$p1);
        $content = str_replace('<table', '<table class="table table-bordered"', $content);

        return $this->render('statistic', ['content' => $content]);
    }

//    public function actionSoap()
//    {
//        $result = Yii::$app->soapClientManager->load('GetStringsPriema', []);
//        var_dump($result);
//        die;
//    }


    public function actionStatistic($form = 'ochnoe')
    {
        $data = [];

        $dir = Yii::getAlias('@frontend') . '/web/uploads/reports/' . $form . '/';
        if (file_exists($dir)) {
            $formNames = [
                'ochnoe' => 'очная',
                'zaochnoe' => 'заочная',
                'ochno-zaochnoe' => 'очно-заочная'
            ];

            $files = array_diff(scandir($dir), array('..', '.'));
            foreach ($files as $file){
                $fileName = $dir . $file;
                if (file_exists($fileName)) {
                    $content = file_get_contents($fileName);
                    $p1 = strpos($content, '<TABLE');
                    $p2 = strpos($content, '</TABLE>');

                    $content = mb_strcut($content, $p1, $p2-$p1+8);
                    $name = str_replace('_', ' ', $file);
                    $name = str_replace('.HTML', '', $name);
                    $name = str_replace('.html', '', $name);
                    $data[] = [
                        'name' => $name,
                        'content' => $content
                    ];
                }
            }
        } else {
//            throw new Exception('Информация по данной форме отсутствует');
            return $this->render('stat', ['data' => 'Информация по данной форме отсутствует']);
        }

        return $this->render('stat', ['data' => $data]);
    }
}
