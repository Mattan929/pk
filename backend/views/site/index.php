<?php

/* @var $this yii\web\View */

$this->title = Yii::$app->name . ': Панель администрирования сайта.';
?>
<div class="site-index">

    <div class="jumbotron">
        <p>
            <a class="btn btn-lg btn-success" href="<?= \yii\helpers\Url::toRoute('/post/create')?>">Создать новость</a>
        </p>
        <p>
            <a class="btn btn-lg btn-success" href="<?= \yii\helpers\Url::toRoute('/page/create')?>">Создать страницу</a>
        </p>

    </div>

    <div class="body-content">


    </div>
</div>
