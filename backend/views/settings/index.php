<?php

use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Настройки';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="schedule-address-index row">
    <div class="col-md-12">
        <p>
            <?= Html::a('Добавить настройку', ['create'], ['class' => 'btn btn-success']) ?>
        </p>

        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],

                'title',
                'value',
                'code',
                [
                    'class' => 'yii\grid\ActionColumn',
                    'template' => '{update} {remove}',
                    'buttons' => [
                        'remove' => function ($url, $model, $key) {
                            if (!$model->cant_be_removed) {
                                $url = Url::to(['settings/delete', 'id' => $key]);
                                return Html::a('<span class="glyphicon glyphicon-trash"></span>', $url, [
                                    'class' => 'btn btn-default',
                                    'title' => \Yii::t('yii', 'Delete'),
                                    'data-confirm' => 'Вы действительно хотите удалить данный элемент?',
                                    'data-method' => 'post',
                                    'data-pjax' => '0',
                                ]);
                            }
                            return '';
                        },
                    ],
                    'buttonOptions' => ['class' => 'btn btn-default'],
                    'options' => ['style' => 'width: 125px;']
                ],
            ],
        ]); ?>
    </div>
</div>
