<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Settings */

$this->title = 'Редактировать настройку: ' . $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Настройки', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Редактирование';
?>
<div class="settings-update">

    <?php if ($model->code === 'social-networks-block') : ?>
        <?= $this->render('_social-networks-form', [
            'model' => $model,
        ]) ?>
    <?php else : ?>
        <?= $this->render('_form', [
            'model' => $model,
        ]) ?>
    <?php endif; ?>

</div>
