<?php

use common\models\Settings;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

/* @var $this \yii\web\View */
/* @var $model common\models\Settings */
/* @var $form yii\widgets\ActiveForm */

?>

<div class="settings-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="row">

        <div class="col-md-4">
            <?= $form->field($model, 'title')->textInput(['maxlength' => true, 'readonly' => $model->cant_be_removed]) ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'code')->textInput(['maxlength' => true, 'readonly' => $model->cant_be_removed]) ?>
        </div>
        <div class="col-md-4">
            <?php if (Yii::$app->user->can('superadmin')) {
                echo $form->field($model, 'cant_be_removed')->checkbox();
            } ?>
        </div>

    </div>

    <div class="row">
        <div class="col-md-12">
            <?= $form->field($model, 'value')->hiddenInput(['data-role' => 'value-field'])->label('Соц. сети:') ?>
            <div class="col-md-3" data-role="add-link-form">
                <?= Html::dropDownList('name', '', Settings::getSocialNetworks()); ?>

                <?= Html::textInput('link', ''); ?>

                <a class="btn btn-default" data-role="add-link-btn" href="#"><i class="fa fa-plus"></i></a>
            </div>
            <div class="col-md-4">
                <ul class="list-unstyled" data-role="social-link-list">
                    <?php foreach (json_decode($model->value) as $social) { ?>
                        <li data-role="social-link" data-name="<?= $social->name ?>" data-link="<?= $social->link ?>">
                                <?= Settings::getSocialNetworks()[$social->name] . ' - ' . $social->link ?>
                                <a class="pull-right" data-role="remove-item" href="#"><i class="fa fa-remove"></i></a>
                        </li>
                        <?php
                    }
                    ?>
                </ul>
            </div>
        </div>
    </div>


    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Создать' : 'Обновить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

