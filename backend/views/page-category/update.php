<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\PageCategory */

$this->title = 'Изменить категорию страниц: ' . $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Категории страниц', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Изменить';
?>
<div class="page-category-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
