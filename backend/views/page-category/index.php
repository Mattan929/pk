<?php

use common\models\PageCategory;
use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\search\PageCategorySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Категории страниц';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="page-category-index">

    <p>
        <?= Html::a('Добавить категорию', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

//            'id',
            'title',
            [
                'attribute' => 'status',
                'filter' => Html::activeDropDownList(
                    $searchModel,
                    'status',
                    PageCategory::getStatusList(),
                    ['class' => 'form-control', 'prompt' => 'Статус']
                ),
                'format' => 'raw',
                'value' => function ($model) {
                    if ($model->status === PageCategory::STATUS_ACTIVE) {
                        return Html::tag('span', 'ВКЛ', [
                            'class' => 'label label-success',
                        ]);
                    }
                    return Html::tag('span', 'ВЫКЛ', [
                        'class' => 'label label-danger',
                    ]);
                }
            ],
//            'created_at',
//            'updated_at',
            //'created_by',
            //'updated_by',

            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{action} {update} {delete}',
                'buttons' => [
                    'action' => function ($url, $model, $key) {
                        if ($model->status === PageCategory::STATUS_ACTIVE) {
                            return Html::a('Отключить', ['disable', 'id' => $model->id], [
                                'class' => 'label label-danger',
                                'title' => 'Нажмите, чтобы включить.',
                                'data-confirm' => 'Вы действительно хотите отключить данный элемент?'
                            ]);
                        }
                        return Html::a('Включить', ['enable', 'id' => $model->id], [
                            'class' => 'label label-success',
                            'title' => 'Нажмите, чтобы отключить.'
                        ]);
                    },
                ],
                'buttonOptions' => ['class' => 'btn btn-default'],
            ],
        ],
    ]); ?>


</div>
