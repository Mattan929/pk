<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\PageCategory */

$this->title = 'Создать категорию страниц';
$this->params['breadcrumbs'][] = ['label' => 'Категории страниц', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="page-category-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
