<aside class="main-sidebar">

    <?php

    use common\models\Question;

    $qtnCount = Question::find()->where(['status' => Question::STATUS_NEW])->count();
    $qtnBadge = '';
    if ($qtnCount > 0) {
        $qtnBadge = '<span class="label pull-right bg-yellow">' . $qtnCount . '</span>';
    }
    ?>
    <section class="sidebar">

        <?= dmstr\widgets\Menu::widget(
            [
                'options' => ['class' => 'sidebar-menu tree', 'data-widget' => 'tree'],
                'items' => [
                    ['label' => 'Меню', 'options' => ['class' => 'header']],
                    ['label' => 'Меню', 'url' => ['/menu/creator']],
                    ['label' => 'Вопросы', 'template' => '<a href="{url}">{icon} {label} ' . $qtnBadge . '</a>', 'url' => ['/question']],
//                    ['label' => 'Сообщения', 'items' => [
//                        ['label' => 'Сообщения', 'url' => ['/comment']],
//                        ['label' => 'Категории сообщений', 'url' => ['/comment-category']],
//                        ]
//                    ],
                    ['label' => 'Слайдер', 'url' => ['/slider']],
                    ['label' => 'Календарь событий', 'url' => ['/event-calendar/event']],
                    ['label' => 'Новости', 'url' => ['/post']],
                    ['label' => 'Страницы', 'url' => ['/page']],
                    ['label' => 'Категории страниц', 'url' => ['/page-category']],
                    ['label' => 'Пользователи', 'icon' => 'users', 'url' => ['/user/admin'], 'visible' => Yii::$app->user->can('moderator')],
                    ['label' => 'Настройки', 'icon' => 'cogs', 'url' => ['/settings'], 'visible' => Yii::$app->user->can('superadmin')],
                ],
            ]
        ) ?>

    </section>

</aside>
