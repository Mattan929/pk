<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\search\CommentSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Сообщения';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="comment-index">

    <!--p>
        <?= Html::a('Добавить', ['create'], ['class' => 'btn btn-success']) ?>
    </p-->

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'rowOptions' => function ($model) {
            if ($model->status === \common\models\Comment::STATUS_NEW) {
                return ['class' => 'warning'];
            }
        },
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

//            'id',
            [
                'attribute' => 'category_id',
                'format' => 'raw',
                'value' => function ($model) {
                    $cat = $model->category;
                    return $cat !== null ? $cat->title : 'Без категории';
                }
            ],
//            'parent_id',
            'name',
            'email:email',
            'contact_phone',
            'body:ntext',
            'status',
            [
                'attribute' => 'created_at',
                'format' => 'raw',
                'value' => function ($model) {
                    return date('d.m.Y h:i:s', $model->created_at);
                }
            ],
            //'updated_at',
            'created_by',
            //'updated_by',

            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{reply} {view} {delete}',
                'buttons' => [
                    'reply' => function ($key, $model){
                        return Html::a('Ответить', 'javascript:', ['class' => 'btn btn-default']);
                    }
                ],
                'buttonOptions' => ['class' => 'btn btn-default']
            ],
        ],
    ]); ?>


</div>
