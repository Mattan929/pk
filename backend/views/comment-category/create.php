<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\CommentCategory */

$this->title = 'Create Comment Category';
$this->params['breadcrumbs'][] = ['label' => 'Comment Categories', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="comment-category-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
