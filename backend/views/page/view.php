<?php

use common\models\Post;
use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Post */

$this->title = 'Предварительный просмотр';
$this->params['breadcrumbs'][] = ['label' => 'Посты', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="post-view">

    <p>
        <?= Html::a('Изменить', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?php
        if ($model->status === Post::STATUS_DRAFT) {
            echo Html::a('Опубликовать', ['publish', 'id' => $model->id], ['class' => 'btn btn-success']);
        }
        if ($model->status === Post::STATUS_PUBLISH) {
            echo Html::a('Скрыть', ['hide', 'id' => $model->id], ['class' => 'btn btn-danger']);
        }
        if ($model->status === Post::STATUS_HIDDEN) {
            echo Html::a('Показать', ['publish', 'id' => $model->id], ['class' => 'btn btn-primary']);
        }
        ?>
        <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?php
    $title = $model->title;
    $description = $model->description;
    ?>
    <div class="post-preview">
        <div class="box">
            <div class="post-title">
                <h1>
                    <?=$title?>
                </h1>
            </div>
            <div class="post-content">
                <?= $description ?>
            </div>
        </div>
    </div>


</div>
