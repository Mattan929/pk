<?php

use common\models\LinkOnPage;
use kartik\depdrop\DepDrop;
use yii\bootstrap\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\web\View;

/* @var $this View */
/* @var $model LinkOnPage */
/* @var $pageId integer */

?>

<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
    <h5 class="modal-title"><?= $model->isNewRecord ? 'Добавить' : 'Редактировать'?> ссылку</h5>
</div>

<?php $form = ActiveForm::begin([
    'id' => 'link-add-form',
    'action' => \yii\helpers\Url::toRoute($model->isNewRecord ? 'create-link' : ['update-link', 'id' => $model->id])
]); ?>

<div class="modal-body">
    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'type')->dropDownList(LinkOnPage::getLinkTypes()) ?>

    <?= $form->field($model, 'url')->textInput(['maxlength' => true]) ?>

    <p>ИЛИ</p>

    <?= $form->field($model, 'page_id')->hiddenInput(['value' => $pageId])->label(false) ?>

    <?= $form->field($model, 'target_class')->widget(\kartik\select2\Select2::className(), [
        'attribute' => 'target_class',
        'options' => [
            'id' => 'target-class',
        ],
        'data' => LinkOnPage::TARGET_CLASSES,
        'pluginOptions' => [
            'allowClear' => true,
            'placeholder' => 'Выберите раздел'
        ]
    ]) ?>

    <?= $form->field($model, 'target_id')->widget(DepDrop::classname(), [
        'options' => [
            'id' => 'target-id'
        ],
        'data' => !empty($model->target_class) ? ArrayHelper::map($model->target_class::find()->all(), 'id', 'title') : [],
        'type' => DepDrop::TYPE_SELECT2,
        'pluginOptions' => [
            'depends' => ['target-class'],
            'placeholder' => 'Выберите элемент раздела...',
            'url' => \yii\helpers\Url::to(['get-items'])
        ]
    ]) ?>

    <?= $form->field($model, 'order')->hiddenInput()->label(false) ?>

</div>
<div class="modal-footer">
    <?= Html::submitButton($model->isNewRecord ? 'Добавить' : 'Сохранить', ['class' => 'btn btn-success']) ?>
    <button type="button" class="btn btn-secondary" data-dismiss="modal">Закрыть</button>
</div>

<?php ActiveForm::end()?>

