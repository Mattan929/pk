<?php

use common\models\Page;
use common\models\PageCategory;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\search\PostSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Страницы';
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="post-index">

    <p>
        <?= Html::a('Добавить страницу', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'title:ntext',
//            'slug',
            [
                'attribute' => 'category_id',
                'filter' => \kartik\select2\Select2::widget([
                    'model' => $searchModel,
                    'attribute' => 'category_id',
                    'data' => ArrayHelper::merge(
                            [0 => 'Без категории'],
                            ArrayHelper::map(PageCategory::findAll(['status' => PageCategory::STATUS_ACTIVE]), 'id', 'title')
                    ),
                    'options' => [
                        'placeholder' => 'Введите название категории',
                    ],
                    'pluginOptions' => [
                        'allowClear' => true
                    ]
                ]),
                'value' => function($model) {
                    if ($cat = $model->category) {
                        return $cat->title;
                    }
                    return 'Без категории';
                }
            ],
            [
                'attribute' => 'status',
                'filter' => Html::activeDropDownList(
                    $searchModel,
                    'status',
                    Page::getStatusList(),
                    ['class' => 'form-control', 'prompt' => 'Статус']
                ),
                'value' => function($model) {
                    $arrayTranslate = Page::getStatusList();
                    return $arrayTranslate[$model->status];
                }
            ],
            [
                'attribute' => 'created_at',
                'filter' => \kartik\date\DatePicker::widget([
                    'model' => $searchModel,
                    'attribute' => 'created_at',
                    'language' => 'ru',
                    'options' => [
                        'class' => 'form-control',
                        'placeholder' => 'Дата создания',
                        'autocomplete' => 'off',
                    ],
                ]),
                'value' => function($model) {
                    return date('d.m.Y H:i:s', $model->created_at);
                }
            ],
            [
                'attribute' => 'link',
                'label' => 'Ссылка',
                'value' => function($model) {
                    return Html::a('Ссылка', Yii::$app->urlManagerFrontend->createUrl('/page/' . $model->id), ['target' => '_blank']);
                },
                'format' => 'raw',
                'visible' => Yii::$app->user->can('moderator')
            ],
            [
                'attribute' => 'on_main_page',
                'value' => function($model) {
                    if ($model->on_main_page) {
                        return Html::tag('span', 'На главной', ['class' => 'label label-success']);
                    }
                },
                'format' => 'raw',
                'visible' => Yii::$app->user->can('moderator')
            ],
            [
                'header' => 'Действия',
                'value' => function($model) {
                    if ($model->status === Page::STATUS_DRAFT) {
                        return Html::a('Опубликовать', ['publish', 'id' => $model->id], ['class' => 'label label-success']);
                    }
                    if ($model->status === Page::STATUS_PUBLISH) {
                        return Html::a('Скрыть', ['hide', 'id' => $model->id], ['class' => 'label label-danger']);
                    }
                    if ($model->status === Page::STATUS_HIDDEN) {
                        return Html::a('Показать', ['publish', 'id' => $model->id], ['class' => 'label label-primary']);
                    }
                },
                'format' => 'raw',
                'visible' => Yii::$app->user->can('moderator')
            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{update} {delete}',
                'buttonOptions' => ['class' => 'btn btn-default'],
            ],
        ],
    ]); ?>
</div>
