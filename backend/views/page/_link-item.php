<?php



/* @var $this \yii\web\View */
/* @var $model \common\models\LinkOnPage */

use yii\helpers\Html;

?>
<li data-position="<?= $model->order ?>">
    <div class="row">
        <div class="col-md-10">
            <?= Html::a($model->title, $model->urlToTarget,
                [
                    'target' => '_blank',
                    'data' => [
                        'role' => 'link',
                        'id' => $model->id,
                    ]
                ]) ?>
        </div>
        <div class="col-md-2">
            <?= Html::a('', 'javascript:',
                [
                    'class' => 'fa fa-pencil',
                    'data' => [
                        'role' => 'open-link-modal',
                        'id' => $model->id,
                        'url' => \yii\helpers\Url::toRoute(['update-link', 'id' => $model->id]),
                    ]
                ]) ?>
            <?= Html::a('', 'javascript:',
                [
                    'class' => 'fa fa-remove',
                    'data' => [
                        'role' => 'delete-link',
                        'id' => $model->id,
                        'url' => \yii\helpers\Url::toRoute(['delete-link', 'id' => $model->id]),
                    ]
                ]) ?>
        </div>
    </div>
</li>
