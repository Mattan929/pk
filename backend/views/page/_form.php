<?php

use common\models\PageCategory;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Post */
/* @var $form yii\widgets\ActiveForm */

$this->registerJsFile('/admin/js/sortable.js');

$js = <<<JS

    $(document).find('#links-block li').each(function (index, elem){
        $(elem).data('position', index).attr('data-position', index);
    });
    Sortable.create(document.getElementById('links-block'), {
      animation: 200,
        // Element dragging ended
      onEnd: function (/**Event*/ evt) {
          var data = {};
          $(document).find('#links-block li').each(function (index, elem){
              $(elem).data('position', index).attr('data-position', index);
              data[$(elem).find('[data-role=link]').data('id')] = index;
          });
          $.ajax({
            url: 'change-order',
            method: 'POST',
            data: data
          });
      },
    });
JS;
if (!$model->isNewRecord){
    $this->registerJs($js);
}
?>
<style>
    .floor12-files-widget-list img {
        max-height: 250px;
    }
</style>
<div class="post-form">

    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

    <div class="row">
        <div class="col-md-4">
            <?= $form->field($model, 'title')->textarea(['rows' => 3]) ?>
            <?= $form->field($model, 'slug')->textInput() ?>
            <?= $form->field($model, 'category_id')->widget(Select2::className(), [
                'data' => ArrayHelper::map(PageCategory::findAll(['status' => PageCategory::STATUS_ACTIVE]), 'id', 'title'),
                'pluginOptions' => [
                        'placeholder' => 'Выберите категорию'
                ]
            ]) ?>
            <div class="row">
                <div class="col-md-3">
                    <?= $form->field($model, 'on_main_page')->checkbox() ?>
                </div>
                <div class="col-md-9">
                    <?= $form->field($model, 'status')->dropDownList(\common\models\Page::getStatusList()) ?>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <label>Ссылки на странице</label>
            <?php if (!$model->isNewRecord): ?>
            <div>
                <ul class="list-unstyled nav nav-pills nav-stacked" id="links-block">
                    <?php foreach ($model->links as $link):
                        /** @var \common\models\LinkOnPage $link */?>
                        <?= $this->render('_link-item', ['model' => $link])?>
                    <?php endforeach; ?>
                </ul>
                <?= Html::a('Добавить ссылку', '#', [
                        'class' => 'btn btn-default form-control',
                        'data' => [
                            'role' => 'open-link-modal',
                            'url' => \yii\helpers\Url::toRoute(['create-link', 'pageId' => $model->id]),
                        ]
                    ]) ?>
            </div>
            <?php else: ?>
            <p>После создания страницы будет доступно размещение ссылок на ней </p>
            <?php endif; ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'image')->widget(floor12\files\components\FileInputWidget::class) ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-9">
            <?= $form->field($model, 'content')->widget(\mihaildev\ckeditor\CKEditor::class, [
                'editorOptions' => [
                    'preset' => 'full', //разработанны стандартные настройки basic, standard, full данную возможность не обязательно использовать
                    'inline' => false, //по умолчанию false
                    'filebrowserUploadUrl' => \yii\helpers\Url::to(['upload-img']),
                    'filebrowserImageUploadUrl' => \yii\helpers\Url::to(['upload-img']),
                    'allowedContent' => true
                ]
            ]) ?>
        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'docs')->widget(floor12\files\components\FileInputWidget::class) ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'short_content')->textarea(['rows' => 4]) ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'description')->textarea(['rows' => 4]) ?>
        </div>
    </div>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Создать' : 'Обновить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<div class="modal fade" data-role="link-modal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">

        </div>
    </div>
</div>