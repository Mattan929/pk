<?php

use common\models\Post;
use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\search\PostSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Посты';
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="post-index">

    <p>
        <?= Html::a('Создать пост', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'title:ntext',
            [
                'attribute' => 'status',
                'filter' => Html::activeDropDownList(
                    $searchModel,
                    'status',
                    Post::getStatusList(),
                    ['class' => 'form-control', 'prompt' => 'Статус']
                ),
                'value' => function($model) {
                    $arrayTranslate = Post::getStatusList();
                    return $arrayTranslate[$model->status];
                }
            ],
            [
                'attribute' => 'created_at',
                'filter' => \kartik\date\DatePicker::widget([
                    'model' => $searchModel,
                    'attribute' => 'created_at',
                    'language' => 'ru',
                    'options' => [
                        'class' => 'form-control',
                        'placeholder' => 'Дата создания',
                        'autocomplete' => 'off',
                    ],
                ]),
                'value' => function($model) {
                    return date('d.m.Y H:i:s', $model->created_at);
                }
            ],
            [
                'attribute' => 'publish_date',
                'filter' => \kartik\date\DatePicker::widget([
                    'model' => $searchModel,
                    'attribute' => 'publish_date',
                    'language' => 'ru',
                    'options' => [
                        'class' => 'form-control',
                        'placeholder' => 'Дата публикации',
                        'autocomplete' => 'off',
                    ],
                ]),
                'value' => function($model) {
                    if ($model->publish_date === null) {
                        return 'не задано';
                    }
                    return date('d.m.Y H:i:s', $model->publish_date);
                }
            ],
            [
                'header' => 'Действия',
                'value' => function($model) {
                    if ($model->status === Post::STATUS_DRAFT) {
                        return Html::a('Опубликовать', ['publish', 'id' => $model->id], ['class' => 'label label-success']);
                    }
                    if ($model->status === Post::STATUS_PUBLISH) {
                        return Html::a('Скрыть', ['hide', 'id' => $model->id], ['class' => 'label label-danger']);
                    }
                    if ($model->status === Post::STATUS_HIDDEN) {
                        return Html::a('Показать', ['publish', 'id' => $model->id], ['class' => 'label label-primary']);
                    }
                },
                'format' => 'raw',
                'visible' => Yii::$app->user->can('moderator')
            ],

            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{update}{delete}',
                'buttonOptions' => ['class' => 'btn btn-default'],
            ],
        ],
    ]); ?>
</div>
