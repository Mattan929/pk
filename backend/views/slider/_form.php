<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Slider */
/* @var $form yii\widgets\ActiveForm */
?>
<style>
    .floor12-files-widget-list img {
        max-height: 250px;
    }
</style>
<div class="slider-form">

    <?php $form = ActiveForm::begin(); ?>
    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'title')->textInput() ?>
            <?= $form->field($model, 'url')->textInput() ?>
            <div class="row">
                <div class="col-md-6">
                    <?= $form->field($model, 'order')->textInput(['type' => 'number', 'value' => $model->isNewRecord ? 0 : $model->order]) ?>
                </div>
                <div class="col-md-6">
                    <?= $form->field($model, 'status')->dropDownList(\common\models\Slider::getStatusList()) ?>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'image')->widget(floor12\files\components\FileInputWidget::class) ?>

            <?= $form->field($model, 'image_mobile')->widget(floor12\files\components\FileInputWidget::class) ?>
        </div>
    </div>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Добавить' : 'Сохранить', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
