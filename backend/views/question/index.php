<?php

use common\models\Question;
use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\search\QuestionSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Вопросы';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="question-index">

    <p>
        <?= Html::a('Добавить вопрос', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'rowOptions' => function ($model) {
            if ($model->status === \common\models\Comment::STATUS_NEW) {
                return ['class' => 'warning'];
            }
        },
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

//            'id',
//            'parent_id',
            'name',
            'email:email',
            'contact_phone',
            'body:ntext',
            [
                'attribute' => 'status',
                'format' => 'raw',
                'filter' => Html::activeDropDownList($searchModel, 'status', Question::getStatusList(), ['class' => 'form-control', 'prompt' => 'Все']),
                'value' => function ($model) {
                    return Html::tag('span', Question::getStatusName($model->status), [
                        'class' => 'label label-' . Question::getStatusColor($model->status),
                    ]);
                }
            ],
            [
                'attribute' => 'created_at',
                'format' => 'raw',
                'value' => function ($model) {
                    return date('d.m.Y h:i:s', $model->created_at);
                }
            ],
            //'updated_at',
            [
                'attribute' => 'created_by',
                'format' => 'raw',
                'value' => function ($model) {
                    if ($user = $model->createdBy) {
                        return $user->username;
                    }
                    return 'Не зарегистрированный пользователь';
                }
            ],
            //'updated_by',

            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{reply} {answered} {view} {delete}',
                'buttons' => [
                    'reply' => function ($key, $model){
                        if ($model->status !== Question::STATUS_NEW) {
                            return '';
                        }
                        return Html::a('Ответить', ['reply', 'id' => $model->id], ['class' => 'btn btn-warning']);
                    },
                    'answered' => function ($key, $model){
                        if ($model->status !== Question::STATUS_NEW) {
                            return '';
                        }
                        return Html::a('Отвечено', ['answered', 'id' => $model->id], ['class' => 'btn btn-info']);
                    }
                ],
                'buttonOptions' => ['class' => 'btn btn-default']
            ],
        ],
    ]); ?>


</div>
