<?php

use common\models\Question;
use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Question */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Questions', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="question-view">

    <p>
        <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
        <?php if($model->status === Question::STATUS_NEW): ?>
        <?= Html::a('Отвечено', ['answered', 'id' => $model->id], ['class' => 'btn btn-info']); ?>
        <?= Html::a('Ответить', ['reply', 'id' => $model->id], ['class' => 'btn btn-warning']) ?>
        <?php endif; ?>
    </p>

    <div class="row">
        <div class="col-lg-6 col-md-12 col-sm-12">
            <h4><strong>Вопрос</strong></h4>
            <?= DetailView::widget([
                'model' => $model,
                'attributes' => [
                    'name',
                    'email:email',
                    'contact_phone',
                    'body:ntext',
                    [
                        'attribute' => 'status',
                        'format' => 'raw',
                        'value' => function ($model) {
                            return Html::tag('span', Question::getStatusName($model->status), [
                                'class' => 'label label-' . Question::getStatusColor($model->status),
                            ]);
                        }
                    ],
                    [
                        'attribute' => 'created_at',
                        'format' => 'raw',
                        'value' => function ($model) {
                            return date('d.m.Y h:i:s', $model->created_at);
                        }
                    ],
                    [
                        'attribute' => 'created_by',
                        'format' => 'raw',
                        'value' => function ($model) {
                            if ($user = $model->createdBy) {
                                return $user->username;
                            }
                            return 'Не зарегистрированный пользователь';
                        }
                    ],
                ],
            ]) ?>

        </div>
        <div class="col-lg-6 col-md-12 col-sm-12">
            <h4><strong>Ответ</strong></h4>
            <?php if($model->answer): ?>
            <?= DetailView::widget([
                'model' => $model->answer,
                'attributes' => [
                    'name',
                    'email:email',
                    'contact_phone',
                    'body:html',
                    [
                        'attribute' => 'created_at',
                        'format' => 'raw',
                        'value' => function ($model) {
                            return date('d.m.Y h:i:s', $model->created_at);
                        }
                    ],
                    [
                        'attribute' => 'created_by',
                        'format' => 'raw',
                        'value' => function ($model) {
                            if ($user = $model->createdBy) {
                                return $user->username;
                            }
                            return 'Не зарегистрированный пользователь';
                        }
                    ],
                ],
            ]) ?>
            <?php else: ?>
            <p>Нет ответа</p>
            <?= Html::a('Ответить', ['reply', 'id' => $model->id], ['class' => 'btn btn-warning']) ?>
            <?php endif; ?>
        </div>
    </div>

</div>
