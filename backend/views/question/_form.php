<?php

use common\models\Question;
use common\models\Settings;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Question */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="question-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'parent_id')->hiddenInput()->label(false) ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true, 'value' => Yii::$app->params['senderName']]) ?>

    <?= $form->field($model, 'email')->textInput(['maxlength' => true, 'value' => Settings::getValue('notification-e-mail')]) ?>

    <?= $form->field($model, 'contact_phone')->hiddenInput(['maxlength' => true])->label(false) ?>

    <?= $form->field($model, 'body')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'status')->hiddenInput(['value' => Question::STATUS_ANSWERED])->label(false) ?>

    <div class="form-group">
        <?= Html::submitButton('Ответить', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
