if (typeof mattan929 == "undefined" || !!!mattan929) {
    var mattan929 = {};
}

mattan929.backendFunctions = {
    init: function () {

        $(document).on('click', '[data-role=open-link-modal]', function (e) {
            var self = $(this),
                url = self.data('url');

            $.ajax({
                url: url,
                success: function (data) {
                    $(document).find('[data-role=link-modal] .modal-content').html(data.content);
                    $(document).find('[data-role=link-modal]').modal('show');
                }
            });
        });

        $(document).on('click', '[data-role=delete-link]', function (e) {
            var self = $(this),
                url = self.data('url');

            if (confirm("Вы действительно хотите удалить данный элемент?")) {
                $.ajax({
                    url: url,
                    success: function (data) {
                        self.closest('li').remove();
                    }
                });
            } else {
                return false;
            }
        });

        $(document).on('beforeSubmit', '#link-add-form', function (e) {
            e.preventDefault();
            var form = $(this),
                url = form.attr('action');

            $.ajax({
                method: 'POST',
                url: url,
                data: form.serialize(),
                success: function (data) {
                    if (data.id) {
                        $(document).find('#links-block li [data-id=' + data.id + ']').closest('li').remove();
                    }
                    $(document).find('#links-block').append(data.content);

                    $(document).find('[data-role=link-modal]').modal('hide');
                }
            });
            return false;
        });

        $(document).on('click', '[data-role=add-link-btn]', function (e) {
            e.preventDefault();
            var name = $(this).closest('[data-role=add-link-form]').find('[name=name]').val(),
                link = $(this).closest('[data-role=add-link-form]').find('[name=link]').val(),
                field = $(document).find('[data-role=value-field]'),
                data = {},
                removeBtn = '<a class="pull-right" data-role="remove-item" href="#"><i class="fa fa-remove"></i></a>';

            var socilals = {
                'instagram': 'Инстаграм',
                'vk': 'Вконтакте',
                'facebook': 'Facebook',
                'whatsapp': 'WhatsApp',
                'viber': 'Viber',
                'twitter': 'Twitter',
                'youtube': 'Youtube',
            };
            var li = $('<li>').html(socilals[name] + ' - ' + link + removeBtn);
            li.data('name', name).attr('data-name', name);
            li.data('link', link).attr('data-link', link);
            $(document).find('[data-role=social-link-list]').append(li);
            $(document).find('[data-role=social-link-list] li').each(function (ind, elem) {
                data[ind] = {name: $(elem).data('name'), link: $(elem).data('link')};
            });

            field.val(JSON.stringify(data));

            return false;
        });

        $(document).on('click', '[data-role=remove-item]', function (e) {
            e.preventDefault();
            var field = $(document).find('[data-role=value-field]'),
                data = {};

            $(this).closest('li').remove();
            $(document).find('[data-role=social-link-list] li').each(function (ind, elem) {
                data[ind] = {name: $(elem).data('name'), link: $(elem).data('link')};
            });
            field.val(JSON.stringify(data));

            return false;
        });
    }
};

mattan929.backendFunctions.init();