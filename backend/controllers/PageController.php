<?php

namespace backend\controllers;

use common\models\LinkOnPage;
use floor12\files\logic\FileCreateFromInstance;
use Yii;
use common\models\Page;
use common\models\search\PageSearch;
use yii\filters\AccessControl;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;
use yii\web\UploadedFile;

/**
 * PageController implements the CRUD actions for Page model.
 */
class PageController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'actions' => [
                            'delete',
                        ],
                        'allow' => true,
                        'roles' => ['superadmin'],
                    ],
                    [
                        'actions' => [
                            'create',
                            'update',
                            'view',
                            'index',
                            'publish',
                            'restore',
                            'hide',
                            'upload-img',
                            'add-link-modal-render',
                            'create-link',
                            'update-link',
                            'delete-link',
                            'get-items',
                            'change-order',
                        ],
                        'allow' => true,
                        'roles' => ['moderator'],
                    ]
                ],
            ],
        ];
    }

    /**
     * Lists all Page models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new PageSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Page model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Page model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Page();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index']);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Page model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index']);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Page model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Page model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Page the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Page::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    public function actionPublish($id)
    {
        $model = $this->findModel($id);
        if (!$model->publish()){
            var_dump($model->getErrors());
        }
        return $this->redirect(['index']);
    }

    public function actionHide($id)
    {
        $model = $this->findModel($id);
        if (!$model->hide()){
            var_dump($model->getErrors());
        }
        return $this->redirect(['index']);
    }

    public function actionRestore($id)
    {
        $this->findModel($id)->publish();
        return $this->redirect(['index']);
    }

    public function actionUploadImg($CKEditorFuncNum)
    {
        $file = UploadedFile::getInstanceByName('upload');
        if ($file){
            $data = array_merge(
                [
                    'modelClass' => 'common\\models\\Page',
                    'attribute' => 'files',
                    'mode' => 'multi',
                    'ratio' => 0
                ],
                Yii::$app->request->post()
            );

            $model = Yii::createObject(FileCreateFromInstance::class, [
                $file,
                $data,
                Yii::$app->user->identity,
            ])->execute();


            if ($model->errors) {
                throw new BadRequestHttpException('Ошибки валидации модели');
            }

            if ($model->save()){

                return '<script type="text/javascript">
                            window.parent.CKEDITOR.tools.callFunction("'.$CKEditorFuncNum.'", "' . $model->href.'", "");
                       </script>';

            } else
                return "Возникла ошибка при загрузке файла\n";
        } else
            return "Файл не загружен\n";
    }

//    public function actionAddLinkModalRender()
//    {
//        Yii::$app->response->format = Response::FORMAT_JSON;
//
//        $model = new LinkOnPage();
//
//        return ['content' => $this->renderAjax('_link-modal', ['model' => $model])];
//    }

    public function actionCreateLink()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $model = new LinkOnPage();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return ['content' => $this->renderAjax('_link-item', ['model' => $model])];
        }
        $pageId = Yii::$app->request->get('pageId', 0);
        $model->order = LinkOnPage::find()->where(['page_id' => $pageId])->count();

        return ['content' => $this->renderAjax('_link-modal', ['model' => $model, 'pageId' => $pageId])];
    }

    public function actionUpdateLink($id)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $model = $this->findLinkModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return ['content' => $this->renderAjax('_link-item', ['model' => $model]), 'id' => $model->id];
        }

        return ['content' => $this->renderAjax('_link-modal', ['model' => $model, 'pageId' => $model->page_id])];
    }

    public function actionDeleteLink($id)
    {
        return $this->findLinkModel($id)->delete();
    }

    /**
     * Finds the Page model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return LinkOnPage the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findLinkModel($id)
    {
        if (($model = LinkOnPage::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    public function actionGetItems() {
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $out = [];
        if (isset($_POST['depdrop_parents'])) {
            $class = $_POST['depdrop_parents'];
            if ($class != null) {
                $out = $class[0]::find()->select(['id', 'title AS name'])->asArray()->all();

                return ['output'=>$out, 'selected'=>''];
            }
        }
        return ['output'=>'', 'selected'=>''];
    }

    public function actionChangeOrder() {

        $data = Yii::$app->request->post();
        $models = LinkOnPage::find()->where(['id' => array_keys($data)])->all();

        $success = true;
        /** @var LinkOnPage $model */
        foreach ($models as $model) {
            $model->order = $data[$model->id];
            if (!$model->save()) {
                $success = false;
            }
        }

        return $success;
    }
}
