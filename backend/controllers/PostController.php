<?php

namespace backend\controllers;

use floor12\files\logic\FileCreateFromInstance;
use Yii;
use common\models\Post;
use common\models\search\PostSearch;
use yii\filters\AccessControl;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;

/**
 * PostController implements the CRUD actions for Post model.
 */
class PostController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'actions' => [
                            'delete',
                        ],
                        'allow' => true,
                        'roles' => ['superadmin'],
                    ],
                    [
                        'actions' => [
                            'create',
                            'update',
                            'view',
                            'index',
                            'publish',
                            'restore',
                            'hide',
                            'upload-img',
                        ],
                        'allow' => true,
                        'roles' => ['moderator'],
                    ]
                ],
            ],
        ];
    }

    /**
     * Lists all Post models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new PostSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Post model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Post model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Post();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            if ((integer)$model->status === Post::STATUS_PUBLISH) {
                $model->publish();
            }
            return $this->redirect(['index', 'id' => $model->id]);
        } else {
//            return var_dump($model->getErrors());
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Post model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            if ((integer)$model->status === Post::STATUS_PUBLISH) {
                $model->publish();
            }
            return $this->redirect(['index', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Post model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Post model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Post the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Post::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionPublish($id)
    {
        $model = $this->findModel($id);
        if (!$model->publish()){
            var_dump($model->getErrors());
        }
        return $this->redirect(['index']);
    }

    public function actionHide($id)
    {
        $model = $this->findModel($id);
        if (!$model->hide()){
            var_dump($model->getErrors());
        }
        return $this->redirect(['index']);
    }

    public function actionRestore($id)
    {
        $this->findModel($id)->publish();
        return $this->redirect(['index']);
    }

    public function actionUploadImg($CKEditorFuncNum)
    {
        $file = UploadedFile::getInstanceByName('upload');
        if ($file){
            $data = array_merge(
                [
                    'modelClass' => 'common\\models\\Post',
                    'attribute' => 'files',
                    'mode' => 'multi',
                    'ratio' => 0
                ],
                Yii::$app->request->post()
            );

            $model = Yii::createObject(FileCreateFromInstance::class, [
                $file,
                $data,
                Yii::$app->user->identity,
            ])->execute();


            if ($model->errors) {
                throw new BadRequestHttpException('Ошибки валидации модели');
            }

            if ($model->save()){
                $ratio = Yii::$app->request->post('ratio', null);

                $view = Yii::$app->request->post('mode') === 'single' ? "_single" : "_file";

                $result = $this->renderAjax('@floor12/files/views/default/' . $view, [
                    'model' => $model,
                    'ratio' => $ratio
                ]);

                return '<script type="text/javascript">
                            window.parent.CKEDITOR.tools.callFunction("'.$CKEditorFuncNum.'", "' . $model->href.'", "");
                       </script>';

            } else
                return "Возникла ошибка при загрузке файла\n";
        } else
            return "Файл не загружен\n";
    }

}
