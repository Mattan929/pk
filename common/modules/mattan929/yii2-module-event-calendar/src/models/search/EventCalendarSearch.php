<?php

namespace mattan929\calendar\models\search;

use mattan929\calendar\models\EventCalendar;
use yii\data\ActiveDataProvider;

class EventCalendarSearch extends EventCalendar
{

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['event_date', 'created_at', 'updated_at', 'post_id'], 'safe'],
            [['title', 'description', 'url'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'id',
            'event_date' => 'Дата события',
            'title' => 'Название',
            'created_at' => 'Создано',
            'updated_at' => 'Обновлено',
            'description' => 'Описание',
            'url' => 'Ссылка на внешний источнок',
            'post_id' => 'Новость на сайте',
            'eventDate' => 'Дата события',
        ];
    }

    public function search($params)
    {
        $query = EventCalendar::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 20,
            ],
            'sort'=> [
                'defaultOrder' => ['event_date' => SORT_DESC]
            ]
        ]);

        $this->load($params);

        if (!$this->validate()) {

            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        if ($this->created_at) {
            $query->andFilterWhere(['between', 'created_at', strtotime($this->created_at), strtotime($this->created_at . ' tomorrow')]);
        }
        if ($this->event_date) {
            $query->andFilterWhere(['between', 'event_date', strtotime($this->event_date), strtotime($this->event_date . ' tomorrow')]);
        }
        if ($this->updated_at) {
            $query->andFilterWhere(['between', 'updated_at', strtotime($this->updated_at), strtotime($this->updated_at . ' tomorrow')]);
        }

        if ($this->created_by) {
            $query->andFilterWhere(['=', 'created_by', $this->created_by]);
        }
        if ($this->updated_by) {
            $query->andFilterWhere(['=', 'updated_by', $this->updated_by]);
        }

        $query->andFilterWhere(['like', 'title', $this->title]);

        return $dataProvider;
    }
}