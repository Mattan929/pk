<?php

namespace mattan929\calendar\models;

use common\models\PageCategory;
use common\models\Post;
use floor12\files\models\File;
use Yii;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;

/**
 * This is the model class for table "event_calendar".
 *
 * @property int $id
 * @property int $event_date
 * @property string $title
 * @property int $created_at
 * @property int $updated_at
 * @property string $description
 * @property string $url
 * @property string $eventDate
 * @property string $urlToTarget
 * @property array $post
 * @property int $post_id
 * @property File $event_img
 * @property array $category
 * @property integer status
 */

class EventCalendar extends \yii\db\ActiveRecord
{
    const MONTHS = [
        1 => 'январь',
        2 => 'февраль',
        3 => 'март',
        4 => 'апрель',
        5 => 'май',
        6 => 'июнь',
        7 => 'июль',
        8 => 'август',
        9 => 'сентябрь',
        10 => 'октябрь',
        11 => 'ноябрь',
        12 => 'декабрь'
    ];

    const MONTHS_ENG = ['',
        1 => 'january',
        2 => 'february',
        3 => 'march',
        4 => 'april',
        5 => 'may',
        6 => 'june',
        7 => 'july',
        8 => 'august',
        9 => 'september',
        10 => 'october',
        11 => 'november',
        12 => 'december'
    ];

    const STATUS_ACTIVE = 1;
    const STATUS_CANCEL = 2;

    public function behaviors()
    {

        return ArrayHelper::merge([
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                ],
            ],
            'blameable' => [
                'class' => BlameableBehavior::class,
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
            'files' => [
                'class' => 'floor12\files\components\FileBehaviour',
                'attributes' => [
                    'event_img'
                ],
            ],
        ], parent::behaviors());
    }

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'event_calendar';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['event_date'], 'required'],
            [['event_date', 'created_at', 'updated_at', 'post_id', 'status'], 'integer'],
            [['title', 'description', 'url'], 'string', 'max' => 255],
            [['url'], 'url'],
            ['event_img', 'file', 'extensions' => ['jpg', 'png', 'jpeg', 'gif'], 'maxFiles' => 1],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'id',
            'event_date' => 'Дата события',
            'title' => 'Название',
            'created_at' => 'Создано',
            'updated_at' => 'Обновлено',
            'description' => 'Описание',
            'url' => 'Ссылка на внешний источнок',
            'post_id' => 'Новость на сайте',
            'eventDate' => 'Дата события',
            'event_img' => 'Изображение',
            'status' => 'Статус',
        ];
    }

    public function getEventDate()
    {
        return !empty($this->event_date) ? date('d.m.Y', $this->event_date) : '';
    }

    public function setEventDate($date)
    {
        if (!empty($date)) {
            $this->event_date = strtotime($date);
            return true;
        }

        return false;
    }

    public static function hasEventToDay($day, $month = null, $year = null)
    {
        if ($month === null) {
            $month = date('m');
        }
        if ($year === null) {
            $year = date('Y');
        }
        $date = $day . '.' . $month . '.' . $year;

        $event = self::find()->where(['between', 'event_date', strtotime($date . ' 00:00:00'), strtotime($date . ' 23:59:59')])->one();

        return !empty($event);
    }

    public static function getEventToDay($day, $month = null, $year = null)
    {
        if ($month === null) {
            $month = date('m');
        }
        if ($year === null) {
            $year = date('Y');
        }
        $date = $day . '.' . $month . '.' . $year;

        $events = self::find()->where(['between', 'event_date', strtotime($date . ' 00:00:00'), strtotime($date . ' 23:59:59')])->all();

        return !empty($events) ? $events : [];
    }

    public function getUrlToTarget()
    {
        if (empty($this->url) && $this->post_id === null) {
            return '#';
        }
        return !empty($this->url) ? Url::to($this->url) : Url::to(['/site/post', 'id' => $this->post_id]);
    }

    public static function getEventToDayListHtml($day, $month = null, $year = null)
    {
        $html = '';
        if ($month === null) {
            $month = date('m');
        }
        if ($year === null) {
            $year = date('Y');
        }

        $events = self::getEventToDay($day, $month, $year);
        if (!empty($events)) {
            foreach ($events as $event) {
                $url = $event->urlToTarget;
                $html .= Html::a($event->title, $url);
                $html .= '<br>';
            }
            return $html;
        }
        return '';
    }

    public function getPost()
    {
        return $this->hasOne(Post::className(), ['id' => 'post_id']);
    }

    static function getDaysInMonthCount($month, $year)
    {
        return $month == 2 ? ($year % 4 ? 28 : ($year % 100 ? 29 : ($year % 400 ? 28 : 29))) : (($month - 1) % 7 % 2 ? 30 : 31);
    }

    public static function getStatusList()
    {
        return [
            self::STATUS_ACTIVE => 'Активно',
            self::STATUS_CANCEL => 'Отменено',
        ];
    }

//    public function getCategory()
//    {
//        if ($this->category_id === 0 || $this->category_id === null) {
//            return [];
//        }
//        return Post::find()->where(['post_type' => 'newscategory', 'id' => $this->category_id])->one();
//    }
}
