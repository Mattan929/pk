<?php

namespace mattan929\calendar\controllers;

use common\models\Post;
use mattan929\calendar\models\EventCalendar;
use mattan929\calendar\widgets\EventCalendarWidget\EventCalendarWidget;
use yii\helpers\Url;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

/**
 * Default controller for the `calendar` module
 */
class DefaultController extends Controller
{
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    public function actionRenderEventCalendar($month, $year, $direction)
    {
        if ($direction === 'next') {
            if ($month >= 12) {
                $month = 1;
                $year++;
            } else {
                $month++;
            }
        }
        if ($direction === 'prev') {
            if ($month <= 1) {
                $month = 12;
                $year--;
            } else {
                $month--;
            }
        }

        return EventCalendarWidget::widget(['year' => $year, 'month' => $month, 'withAsset' => false]);
    }

    public function actionEvents($date = null)
    {
        if ($date !== null) {
            $events = EventCalendar::find()
                ->where(
                    [
                        'between',
                        'event_date',
                        strtotime($date . ' 00:00:00'),
                        strtotime($date . ' 23:59:59')
                    ]
                )
                ->orderBy(['event_date' => SORT_ASC])
                ->all();
        } else {
            $events = EventCalendar::find()
                ->where(
                    [
                        '>=',
                        'event_date',
                        strtotime($date . ' 00:00:00'),
                    ]
                )
                ->orderBy(['event_date' => SORT_ASC])
                ->all();
        }

        return $this->render('event', [
            'events' => $events,
        ]);
    }

    public function actionEventUrl($id)
    {
        $model = $this->findModel($id);

        return $this->redirect($model->urlToTarget);
    }


    /**
     * Finds the EventCalendar model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return EventCalendar the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = EventCalendar::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
