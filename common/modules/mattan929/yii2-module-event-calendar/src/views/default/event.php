<?php

use mattan929\calendar\widgets\EventCalendarWidget\EventsList;

/* @var $this \yii\web\View */
/* @var $events  */
$this->title = 'Календарь событий';
$this->params['breadcrumbs'][] = ['label' => $this->title, 'url' => ['/event-calendar/default/events']];
?>
<br>
<?= EventsList::widget(['events' => $events])?>

