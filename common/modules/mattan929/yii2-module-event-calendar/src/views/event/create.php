<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model mattan929\calendar\models\EventCalendar */

$this->title = 'Создать событие в календаре';
$this->params['breadcrumbs'][] = ['label' => 'События', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="event-calendar-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
