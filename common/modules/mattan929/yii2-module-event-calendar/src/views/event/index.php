<?php

use kartik\date\DatePicker;
use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $searchModel \mattan929\calendar\models\search\EventCalendarSearch */

$this->title = 'События';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="event-calendar-index">

    <p>
        <?= Html::a('Создать событие в календаре', ['create'], ['class' => 'btn btn-success']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            [
                'attribute' => 'eventDate',
                'filter' => DatePicker::widget([
                    'model' => $searchModel,
                    'language' => 'ru-RU',
                    'type' => DatePicker::TYPE_INPUT,
                    'attribute' => 'event_date',
                    'options' => ['placeholder' => 'Введите дату создания'],
                    'pluginOptions' => [
                        'autoclose' => true,
                        'format' => 'dd.mm.yyyy',
                        'todayHighlight' => true
                    ]
                ]),
            ],
            'title',
            [
                'attribute' => 'created_at',
                'filter' => DatePicker::widget([
                    'model' => $searchModel,
                    'language' => 'ru-RU',
                    'type' => DatePicker::TYPE_INPUT,
                    'attribute' => 'created_at',
                    'options' => ['placeholder' => 'Введите дату создания'],
                    'pluginOptions' => [
                        'autoclose' => true,
                        'format' => 'dd.mm.yyyy',
                        'todayHighlight' => true
                    ]
                ]),
                'value' => function($model){
                    return date('d.m.Y H:i:s', $model->created_at);
                }
            ],
            [
                'attribute' => 'updated_at',
                'filter' => DatePicker::widget([
                    'model' => $searchModel,
                    'language' => 'ru-RU',
                    'type' => DatePicker::TYPE_INPUT,
                    'attribute' => 'updated_at',
                    'options' => ['placeholder' => 'Введите дату создания'],
                    'pluginOptions' => [
                        'autoclose' => true,
                        'format' => 'dd.mm.yyyy',
                        'todayHighlight' => true
                    ]
                ]),
                'value' => function($model){
                    return date('d.m.Y H:i:s', $model->updated_at);
                }
            ],

            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{update} {delete}',
                'buttonOptions' => ['class' => 'btn btn-default']
            ],
        ],
    ]); ?>


</div>
