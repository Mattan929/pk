<?php

use common\models\Post;
use floor12\files\components\FileInputWidget;
use mattan929\calendar\models\EventCalendar;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model mattan929\calendar\models\EventCalendar */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="event-calendar-form">
<div class="row">
    <?php $form = ActiveForm::begin(); ?>
    <div class="col-md-8">
        <?= $form->field($model, 'eventDate')->widget(\kartik\date\DatePicker::class, [
            'options' => [
                'placeholder' => 'Выберите дату ...',
                'autocomplete' => 'off'
            ],
            'language' => 'ru-RU',
            'pluginOptions' => [
                'format' => 'dd.mm.yyyy',
                'todayHighlight' => true,
                'autoclose' => true,
                'startDate' => '0d',
//            'endDate' => '0d'
            ]
        ]) ?>

        <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'description')->textarea(['rows' => 4, 'maxlength' => true]) ?>

        <?= $form->field($model, 'post_id')->widget(\kartik\select2\Select2::class, [
            'data' => \yii\helpers\ArrayHelper::map(Post::find()
                ->where(['<>', 'title', ''])
                ->andWhere(['status' => Post::STATUS_PUBLISH])
                ->all(),
                'id', 'title'),
            'options' => [
                'prompt' => 'Выберите новость из списка'
            ]
        ]) ?>

        <p>ИЛИ</p>

        <?= $form->field($model, 'url')->textInput(['maxlength' => true]) ?>

        <div class="form-group">
            <?= Html::submitButton($model->isNewRecord ? 'Добавить' : 'Сохранить', ['class' => 'btn btn-success']) ?>
        </div>
    </div>
    <div class="col-md-4">
        <?= $form->field($model, 'event_img')->widget(FileInputWidget::class) ?>

        <?= $form->field($model, 'status')->dropDownList(EventCalendar::getStatusList()) ?>

    </div>

    <?php ActiveForm::end(); ?>
</div>

</div>
