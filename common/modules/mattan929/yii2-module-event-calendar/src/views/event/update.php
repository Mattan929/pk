<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model mattan929\calendar\models\EventCalendar */

$this->title = 'Изменить событие: ' . $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Event Calendars', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Редактирование';
?>
<div class="event-calendar-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
