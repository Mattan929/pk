<?php

use mattan929\calendar\models\EventCalendar;
use yii\db\Migration;

/**
 * Class m210106_165618_calendar
 */
class m210106_165618_event_calendar extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%event_calendar}}', [
            'id' => $this->primaryKey(),
            'event_date' => $this->integer(11)->notNull(),
            'title' => $this->string()->null(),
            'created_at' => $this->integer(11)->notNull(),
            'updated_at' => $this->integer(11)->null(),
            'description' => $this->string()->null(),
            'url' => $this->string(),
            'post_id' => $this->integer(),
            'created_by' => $this->integer(),
            'updated_by' => $this->integer(),
            'status' => $this->smallInteger()->defaultValue(EventCalendar::STATUS_ACTIVE)
        ]);

        $this->createIndex('{{%idx-event_date}}','{{%event_calendar}}','event_date');
        $this->createIndex('{{%idx-post_id}}','{{%event_calendar}}','post_id');
        $this->createIndex('{{%idx-created_by}}','{{%event_calendar}}','created_by');
        $this->createIndex('{{%idx-updated_by}}','{{%event_calendar}}','updated_by');
        $this->createIndex('{{%idx-event_date-post_id}}','{{%event_calendar}}',['event_date', 'post_id']);

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%event_calendar}}');
    }
}
