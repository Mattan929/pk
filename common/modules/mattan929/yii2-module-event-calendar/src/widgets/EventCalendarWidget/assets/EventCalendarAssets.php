<?php

namespace mattan929\calendar\widgets\EventCalendarWidget\assets;

class EventCalendarAssets extends \yii\web\AssetBundle
{
    public $depends = [
        'frontend\assets\AppAsset'
    ];

    public $js = [
        'js/event-calendar.js'
    ];

    public $css = [
        'css/event-calendar.css'
    ];

    public function init()
    {
        $this->sourcePath = dirname(__FILE__) . '/../web';

        parent::init();
    }
}