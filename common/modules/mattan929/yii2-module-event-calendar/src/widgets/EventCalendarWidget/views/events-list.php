<?php

/* @var $this \yii\web\View */

use yii\helpers\Url;

/* @var $events \mattan929\calendar\models\EventCalendar*/


foreach ($events as $event) {
    /** @var \common\models\Post $news */
    $news = $event->post;
    $image = $event->event_img;
    if ($image === null && $news !== null) {
        $image = $news->post_image;
    }
    ?>

    <div class="col-md-4">
        <div class="panel">
            <div class="panel-header">
                <div class="flag-title">
                    <?= Yii::$app->formatter->asDate($event->event_date, 'dd MMMM Y') ?>
                </div>
                <div class="img-block">
                    <img src="<?= $image !== null ? $image->getPreviewWebPath(396, 0, true) : '' ?>" alt="">
                </div>
            </div>
            <div class="panel-body">
                <div class="text">
                    <a href="<?= $event->urlToTarget ?>">
                        <h4 class="postdesc"><?= $event->title ?></h4>
                    </a>
                    <p>
                        <?= $event->description ?>
                    </p>
                </div>
            </div>
        </div>
    </div>

<?php } ?>