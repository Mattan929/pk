<?php

use mattan929\calendar\models\EventCalendar;

/* @var $this \yii\web\View
 * @var $events EventCalendar
 * @var $month integer
 * @var $year integer
 * @var string $monthName
 * @var DateTime $firstDay
 * @var DateTime $lastDay
 * @var boolean $onlyCalendar
 */

?>
<div class="event-calendar" data-role="event-calendar-block">
    <div class="col-sm-12 col-md-4 col-lg-4 calendar">
        <div class="title-block">
            <p class="text-uppercase">Календарь событий</p>
            <a href="<?= \yii\helpers\Url::toRoute('/event-calendar/default/events') ?>">Все события</a>
        </div>
        <div class="calendar-block">
            <div class="jzdbox1 jzdbasf jzdcal">
                <div class="jzdcalt">
                    <!--?= \yii\helpers\Html::dropDownList('', $month, EventCalendar::MONTHS)?-->
                    <a class="pull-left" role="button"
                       data-role="prev-month"
                       data-current-month="<?= $month ?>"
                       data-current-year="<?= $year ?>"
                       data-url="<?= \yii\helpers\Url::toRoute('/event-calendar/default/render-event-calendar') ?>">
                        <i class="glyphicon glyphicon-menu-left" aria-hidden="true"></i>
                        <span class="sr-only">Предыдущий месяц</span>
                    </a>

                    <?= $monthName . ' ' . $year ?>
                    <a class="pull-right" role="button"
                       data-role="next-month"
                       data-current-month="<?= $month ?>"
                       data-current-year="<?= $year ?>"
                       data-url="<?= \yii\helpers\Url::toRoute('/event-calendar/default/render-event-calendar') ?>">
                        <i class="glyphicon glyphicon-menu-right" aria-hidden="true"></i>
                        <span class="sr-only">Следующий месяц</span>
                    </a>
                </div>
                <?php
                $dayNumber = (integer)$firstDay->format('w');
                if ($dayNumber === 0) {
                    $dayNumber = 7;
                }

                for ($i = 1; $i < $dayNumber; $i++) {
                    echo '<span class="jzdb"><!--BLANK--></span>';
                }
                ?>

                <?php

                for ($day = 1; $day <= EventCalendar::getDaysInMonthCount($month, $year); $day++) {
                    if (EventCalendar::hasEventToDay($day, $month, $year)) {
                        $dayHtml = \yii\helpers\Html::a($day, \yii\helpers\Url::toRoute([
                            '/event-calendar/default/events',
                            'date' => $day . '.' . ($month < 10 ? '0' . $month : $month) . '.' . $year
                        ]));
                    } else {
                        $dayHtml = $day;
                    }
                    echo \yii\helpers\Html::tag('span', $dayHtml, [
                        'class' => EventCalendar::hasEventToDay($day, $month, $year) ? 'circle' : '',
//                                'title' => EventCalendar::hasEventToDay($day, $month, $year) ? EventCalendar::getEventToDayListHtml($day, $month, $year) : '',
//                                'data-toggle' => 'tooltip',
//                                'data-html' => true
                    ]);
                }
                ?>

                <?php
                $dayNumber = (integer)$lastDay->format('w');
                for ($i = $dayNumber; $i <= 6; $i++) {
                    echo '<span class="jzdb"><!--BLANK--></span>';
                }
                ?>
            </div>
        </div>
    </div>
    <div class=" col-xs-12 col-sm-12 col-md-8 col-lg-8" style="padding: 0">
        <div id="event-carousel" class="carousel slide" data-interval="false" data-wrap="false">
            <div class="carousel-inner">
                <?php
                foreach ($events as $key => $triadEvent) {
                    ?>
                    <div class="item <?= $key === 0 ? 'active' : '' ?>">
                        <?php
                        /** @var EventCalendar $event */
                        foreach ($triadEvent as $event) { ?>
                            <div class="col-sm-12 col-md-4 col-lg-4 event"
                                 onClick="window.location='<?= \yii\helpers\Url::toRoute(['/event-calendar/default/event-url', 'id' => $event->id]) ?>'">
                                <p class="event-date">
                                    <?= Yii::$app->formatter->asDate($event->event_date, 'dd.MM.Y') ?>
                                </p>
                                <p class="event-title">
                                    <?= \yii\helpers\Html::encode($event->title) ?>
                                </p>
                                <div class="event-description">
                                    <?= \yii\helpers\Html::encode($event->description) ?>
                                </div>
                            </div>
                        <?php }
                        ?>
                    </div>
                    <?php
                }
                ?>
            </div>
            <a class="left carousel-control" role="button"
               data-role="prev-month"
               data-current-month="<?= $month ?>"
               data-current-year="<?= $year ?>"
               data-url="<?= \yii\helpers\Url::toRoute('/event-calendar/default/render-event-calendar') ?>">
                <span class="glyphicon glyphicon-menu-left" aria-hidden="true"></span>
                <span class="sr-only">Предыдущий месяц</span>
            </a>
            <a class="right carousel-control" style="<?= count($events) > 1 ? 'display: none' : '' ?>" role="button"
               data-role="next-month"
               data-current-month="<?= $month ?>"
               data-current-year="<?= $year ?>"
               data-url="<?= \yii\helpers\Url::toRoute('/event-calendar/default/render-event-calendar') ?>">
                <span class="glyphicon glyphicon-menu-right" aria-hidden="true"></span>
                <span class="sr-only">Следующий месяц</span>
            </a>
            <?php
            if (count($events) > 1) { ?>
                <!-- Элементы управления -->
                <a class="left carousel-control" style="display: none" href="#event-carousel" role="button"
                   data-slide="prev">
                    <span class="glyphicon glyphicon-menu-left" aria-hidden="true"></span>
                    <span class="sr-only">Предыдущий</span>
                </a>
                <a class="right carousel-control" href="#event-carousel" role="button" data-slide="next">
                    <span class="glyphicon glyphicon-menu-right" aria-hidden="true"></span>
                    <span class="sr-only">Следующий</span>
                </a>
                <?php
            }
            ?>
        </div>
    </div>
</div>
<div class="clearfix"></div>
