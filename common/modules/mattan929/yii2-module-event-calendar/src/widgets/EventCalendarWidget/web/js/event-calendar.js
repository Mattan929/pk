if(typeof mattan929 == "undefined" || !mattan929) {
    var mattan929 = {};
}

mattan929.eventCalendar = {

    init: function () {

        $(document).on('slide.bs.carousel', '#event-carousel', function (e) {

            var prevBtn = $(document).find('#event-carousel [data-slide=prev]'),
                nextBtn = $(document).find('#event-carousel [data-slide=next]'),
                nextMonthBtn = $(document).find('#event-carousel [data-role=next-month]'),
                prevMonthBtn = $(document).find('#event-carousel [data-role=prev-month]'),
                to,
                from,
                count = $(e.relatedTarget).parent().find('.item').length - 1;

            $(e.relatedTarget).parent().find('.item').each(function (index, element) {
                if (element === e.relatedTarget) {
                    to = index;
                }
            });
            if (e.direction === "left") {
                from = (to === 0) ? count : to - 1;
            } else {
                from = (to === count) ? 0 : to + 1;
            }

            if (e.direction === 'left'){
                prevBtn.show();
                prevMonthBtn.hide();
            }
            if (e.direction === 'right'){
                nextBtn.show();
                nextMonthBtn.hide();
            }


            if (e.direction === 'left' && to === count) {
                nextBtn.hide();
                nextMonthBtn.show();
            }
            if (to === 0 && e.direction === 'right') {
                prevBtn.hide();
                prevMonthBtn.show();
            }
        });

        $(document).on('click', '[data-role=next-month]', function (e) {
            var month = $(this).data('current-month'),
                year = $(this).data('current-year'),
                url = $(this).data('url');

            $.ajax({
                url: url,
                data: {
                    month: month,
                    year: year,
                    direction: 'next'
                },
                success: function (data) {
                    $(document).find('[data-role=event-calendar-block]').before(data).remove();
                }
            });
        });

        $(document).on('click', '[data-role=prev-month]', function (e) {
            var month = $(this).data('current-month'),
                year = $(this).data('current-year'),
                url = $(this).data('url');

            $.ajax({
                url: url,
                data: {
                    month: month,
                    year: year,
                    direction: 'prev'
                },
                success: function (data) {
                    $(document).find('[data-role=event-calendar-block]').before(data).remove();
                }
            });
        });

    }
},

mattan929.eventCalendar.init();