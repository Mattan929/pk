<?php

namespace common\components\soap;

use Yii;
use SoapClient;
use common\components\soap\models\DebuggingSoap;

class soapClientManager extends \yii\base\Component
{
    public $wsdl;
    public $login;
    public $password;
    public $debug;

    public $client;

    public function init()
    {
        parent::init();
        try {
            ini_set('default_socket_timeout', 600);
            $this->client = new SoapClient($this->wsdl, [
                'login' => $this->login,
                'password' => $this->password,
                                'keep_alive' => false,
                'trace' => false,
                'exceptions' => true,
                'cache_wsdl' => WSDL_CACHE_NONE
            ]);
        } catch (\Exception $e) {
            Yii::$app->session->setFlash(
                'UnsuccessfulConnectionWithSoap',
                '<b>Невозможно подключиться к веб-сервисам 1С:Университет:</b> ' . $e->getMessage()
            );
            throw new \yii\base\UserException('Документ WSDL недоступен или некорректен.', '11001', $e);
            die;
        }
    }

    public function load($action, $params = null)
    {
        $startTime = date(DATE_ATOM);
        $debuggingEnable = 0;
        try {
            $model = DebuggingSoap::findOne(['id' => 1]);
            $debuggingEnable = $model->debugging_enable;
        } catch (\Exception $e) {
            $debuggingEnable = 0;
            Yii::error('Не установлена таблица "debuggingsoap"');
        }
        try {
            if ($this->client != null) {
                $buffer = $this->client->$action($params);
            } else {
                $buffer = false;
            }
            if ($debuggingEnable == 1) {
                if($action == 'PutFilePart') {
                    unset($params['PartData']);
                }
                if ($action == 'SaveAttachedFile') {
                    Yii::warning("Начало обращение $startTime к методу $action", $action);
                } else {
                    Yii::warning("Начало обращение $startTime к методу $action " . PHP_EOL . print_r($params, true), $action);
                }
                if ($action == 'GetBinaryData') {
                    Yii::warning('Полученный ответ Бинарный', $action);
                } else {
                    Yii::warning('Полученный ответ ' . PHP_EOL . print_r($buffer, true), $action);
                }
            }
            return $buffer;
        } catch (\Exception $e) {
            if($action == 'PutFilePart') {
                unset($params['PartData']);
            }
            Yii::error('Ошибка обращения к методу ' . $action . ' (' . $e->getMessage() . ').' . PHP_EOL . print_r($params, true));
            Yii::error("Начало обращение к методу $startTime");
            throw new soapException(
                'Ошибка обращения к методу.',
                '11002',
                $action,
                $e->getMessage(),
                $params
            );
        }
    }
}
