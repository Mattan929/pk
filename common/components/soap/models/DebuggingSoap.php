<?php
namespace common\components\soap\models;

use yii\db\ActiveRecord;

class DebuggingSoap extends ActiveRecord
{
    public static function tableName()
    {
        return '{{%debuggingsoap}}';
    }

    public function rules()
    {
        return [
            [['debugging_enable'], 'integer'],
        ];
    }

    
    public function attributeLabels()
    {
        return [
            'debugging_enable' => 'Включить логирование СОАП',
        ];
    }
}