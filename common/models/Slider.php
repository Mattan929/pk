<?php

namespace common\models;

use floor12\files\models\File;
use Yii;

/**
 * This is the model class for table "slider".
 *
 * @property int $id
 * @property string|null $title
 * @property string|null $url
 * @property int|null $order
 * @property int $status
 * @property File $image
 * @property File $image_mobile
 */
class Slider extends \yii\db\ActiveRecord
{

    const STATUS_ACTIVE = 1;
    const STATUS_INACTIVE = 0;

    function behaviors()
    {
        return [
            'files' => [
                'class' => 'floor12\files\components\FileBehaviour',
                'attributes' => [
                    'image' => [
                        'ratio' => '1280/400'
                    ],
                    'image_mobile' => [
                        'ratio' => '375/400'
                    ],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'slider';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['title'], 'required'],
            [['title', 'url'], 'string'],
            ['image', 'file', 'extensions' => ['jpg', 'png', 'jpeg', 'gif'], 'maxFiles' => 1],
            ['image_mobile', 'file', 'extensions' => ['jpg', 'png', 'jpeg', 'gif'], 'maxFiles' => 1],
            [['order', 'status'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Заголовок',
            'url' => 'Ссылка',
            'order' => 'Порядок',
            'status' => 'Статус',
            'image' => 'Изображение',
            'image_mobile' => 'Изображение для телефонов',
        ];
    }

    public static function getStatusList()
    {
        return [
            self::STATUS_ACTIVE => 'вкл',
            self::STATUS_INACTIVE => 'выкл',
        ];
    }

    public function enable()
    {
        $this->status = self::STATUS_ACTIVE;
        return $this->save();
    }

    public function disable()
    {
        $this->status = self::STATUS_INACTIVE;
        $this->order = null;
        return $this->save();
    }

    public function setOrder($ord)
    {
        $this->order = $ord;
        return $this->save();
    }
}
