<?php

namespace common\models;

use Yii;
use yii\helpers\Url;
use common\models\Page;
use common\models\Post;
use mattan929\calendar\models\EventCalendar;

/**
 * This is the model class for table "link_on_page".
 *
 * @property int $id
 * @property string $title
 * @property string|null $url
 * @property int|null $type
 * @property int|null $page_id
 * @property int|null $target_id
 * @property string|null $target_class
 * @property-read string $urlToTarget
 * @property int|null $order
 */
class LinkOnPage extends \yii\db\ActiveRecord
{

    const TARGET_CLASSES = [
        Page::class => 'Страницы',
        Post::class => 'Новости',
        EventCalendar::class => 'События',
    ];

    const TARGET_CLASSES_URL = [
        Page::class => '/site/page',
        Post::class => '/site/post',
        EventCalendar::class => '/event-calendar/default/event-url',
    ];

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'link_on_page';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['title'], 'required'],
            [['page_id', 'target_id', 'order'], 'integer'],
            [['type', 'title', 'url', 'target_class'], 'string', 'max' => 255],
            [['url'], 'url'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'title' => 'Заголовок',
            'url' => Yii::t('app', 'Url'),
            'type' => 'Тип',
            'page_id' => Yii::t('app', 'Page ID'),
            'target_id' => 'Элемент',
            'target_class' => 'Раздел',
            'order' => Yii::t('app', 'Order'),
        ];
    }

    public function getUrlToTarget()
    {
        if (empty($this->url) && $this->target_id === null) {
            return '#';
        }
        return !empty($this->url) ?
            Url::to($this->url) :
            Url::toRoute([self::TARGET_CLASSES_URL[$this->target_class], 'id' => $this->target_id]);
    }

    public static function getLinkTypes()
    {
        return [
            '' => 'В этой же вкладке',
            '_blank' => 'В новой вкладке'
        ];
    }
}
