<?php

namespace common\models;

use floor12\files\models\File;
use Yii;
use yii\helpers\Html;

/**
 * This is the model class for table "settings".
 *
 * @property int $id
 * @property string $title
 * @property string $value
 * @property string $additional_value
 * @property int|null $cant_be_removed
 * @property string|null $code
 * @property File[] $documents
 * @property File[] $application
 * @property File[] $confirmation
 * @property File[] $contract_offer
 */
class Settings extends \yii\db\ActiveRecord
{
    function behaviors()
    {
        return [
//            'files' => [
//                'class' => 'floor12\files\components\FileBehaviour',
//                'attributes' => [
//                    'documents',
//                ],
//            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'settings';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['title', 'value'], 'required'],
            [['value', 'additional_value'], 'string'],
            [['cant_be_removed'], 'integer'],
            [['title', 'code'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Название',
            'value' => 'Значение',
            'additional_value' => 'Дополнительное значение',
            'cant_be_removed' => 'Без возможности удалить',
            'code' => 'Код',
//            'documents' => 'Документы',
        ];
    }

    public static function getValue($code)
    {
        $setting = self::findOne(['code' => $code]);
        if ($setting !== null) {
            if ($setting->code === 'social-networks-block') {
                return json_decode($setting->value);
            }
            return $setting->value;
        }
        return '';
    }

    public static function getSocialNetworks()
    {
        return [
            'instagram' => 'Инстаграм',
            'vk' => 'Вконтакте',
            'facebook' => 'Facebook',
            'whatsapp' => 'WhatsApp',
            'viber' => 'Viber',
            'twitter' => 'Twitter',
            'youtube' => 'Youtube',
        ];
    }
}
