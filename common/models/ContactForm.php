<?php

namespace common\models;

use Swift_TransportException;
use Yii;
use yii\base\Model;

/**
 * ContactForm is the model behind the contact form.
 */
class ContactForm extends Model
{
    public $name;
    public $email;
    public $subject;
    public $contact_phone;
    public $body;
    public $captcha;

    const SCENARIO_CONTACT = 'contact';

    public function scenarios()
    {
        $fields = array_keys($this->attributeLabels());
        return [
            self::SCENARIO_DEFAULT => $fields,
            self::SCENARIO_CONTACT => $fields,
        ];
    }

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            // name, email, subject and body are required
            [['name', 'email', 'body'], 'required', 'on' => self::SCENARIO_DEFAULT],
            [['name', 'email', 'body', 'verifyCode'], 'required', 'on' => self::SCENARIO_CONTACT],
            [['subject', 'contact_phone'], 'string'],
            // email has to be a valid email address
            ['email', 'email'],
            // verifyCode needs to be entered correctly
            ['captcha', 'captcha', 'captchaAction' => '/site/captcha'],

        ];
    }

    /**
     * @return array customized attribute labels
     */
    public function attributeLabels()
    {
        return [
            'name' => 'Фамилия Имя Отчество',
            'email' => 'Адрес электронной почты',
            'subject' => 'Тема письма',
            'contact_phone' => 'Контактный телефон',
            'body' => 'Текст обращения',
            'captcha' => 'captcha',
        ];
    }

    /**
     * Sends an email to the specified email address using the information collected by this model.
     * @param string $email the target email address
     * @return bool whether the model passes validation
     */
    public function contact($email)
    {
        if ($this->validate()) {
            $comment = new Question();
            $comment->name = $this->name;
            $comment->contact_phone = $this->contact_phone;
            $comment->email = $this->email;
            $comment->body = $this->body;
            $comment->save();

            $mailText = 'ФИО: ' . $this->name . PHP_EOL;
            $mailText .= $this->email !== null ? 'e-mail: ' . $this->email . PHP_EOL : '';
            $mailText .= $this->contact_phone !== null ? 'Контактный телефон: ' . $this->contact_phone . PHP_EOL : '';
            $mailText .= $this->body !== null ? 'Текст обращения: ' . $this->body . PHP_EOL : '';

            Yii::$app->mailer->compose()
                ->setTo($email)
                ->setFrom([Yii::$app->params['senderEmail'] => Yii::$app->params['senderName']])
                ->setSubject('Форма обратной связи')
                ->setTextBody($mailText)
                ->send();

            return true;
        }
        return false;
    }
}
