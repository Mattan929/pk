<?php

namespace common\models;

use floor12\files\models\File;
use voskobovich\behaviors\ManyToManyBehavior;
use Yii;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "page".
 *
 * @property int $id
 * @property string $title
 * @property string $slug
 * @property string|null $content
 * @property string|null $short_content
 * @property string|null $description
 * @property int|null $status
 * @property int|null $on_main_page
 * @property int|null $created_at
 * @property int|null $updated_at
 * @property int|null $created_by
 * @property int|null $category_id
 * @property int|null $updated_by
 * @property File $image
 * @property-read mixed $links
 * @property File $docs
 */
class Page extends \yii\db\ActiveRecord
{
    const STATUS_DRAFT = 0;
    const STATUS_PUBLISH = 1;
    const STATUS_HIDDEN = 2;


    function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                ],
            ],
            'blameable' => [
                'class' => BlameableBehavior::class,
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
            'slug' => [
                'class' => 'Zelenin\yii\behaviors\Slug',
                'slugAttribute' => 'slug',
                'attribute' => 'title',
                // optional params
                'ensureUnique' => true,
                'replacement' => '-',
                'lowercase' => true,
                'immutable' => false,
                // If intl extension is enabled, see http://userguide.icu-project.org/transforms/general.
                'transliterateOptions' => 'Russian-Latin/BGN; Any-Latin; Latin-ASCII; NFD; [:Nonspacing Mark:] Remove; NFC;'
            ],
            'files' => [
                'class' => 'floor12\files\components\FileBehaviour',
                'attributes' => [
                    'image',
                    'docs',
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'page';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['title'], 'required'],
            [['content', 'description', 'short_content'], 'string'],
            [['status', 'on_main_page', 'created_at', 'updated_at', 'created_by', 'updated_by', 'category_id'], 'integer'],
            ['image', 'file', 'extensions' => ['jpg', 'png', 'jpeg', 'gif'], 'maxFiles' => 1],
            ['docs', 'file', 'extensions' => ['pdf', 'doc', 'docx', 'xls', 'xlsx', 'rtf'], 'maxFiles' => 50],
            [['title', 'slug'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Заголовок',
            'slug' => 'ЧПУ',
            'content' => 'Контент',
            'short_content' => 'Краткое описание',
            'description' => 'Доп. информация',
            'status' => 'Статус',
            'category_id' => 'Категория',
            'created_at' => 'Создано в',
            'updated_at' => 'Обновлено в',
            'created_by' => 'Создал',
            'updated_by' => 'Обновил',
            'on_main_page' => 'Отображать на главной',
            'image' => 'Изображение',
            'docs' => 'Документы',
            'link_ids' => 'Ссылки',
        ];
    }

    public function getLinks()
    {
        return $this->hasMany(LinkOnPage::className(), ['page_id' => 'id'])->orderBy('order');
    }

    public static function getStatusList()
    {
        return [self::STATUS_DRAFT => 'Черновик', self::STATUS_PUBLISH => 'Опубликован', self::STATUS_HIDDEN => 'Скрыт'];
    }

    public function publish()
    {
        $this->status = self::STATUS_PUBLISH;
        return $this->save();
    }

    public function hide()
    {
        $this->status = self::STATUS_HIDDEN;
        return $this->save();
    }

    /**
     * Gets query for [[Pages]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCategory()
    {
        return $this->hasOne(PageCategory::className(), ['id' => 'category_id']);
    }
}
