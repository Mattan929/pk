<?php

namespace common\models;

use yii\base\Model;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\httpclient\Client;
use yii\web\NotFoundHttpException;

class AreaInfo extends Model
{

    public $subjs = '';
    public $form = 1;
    public $pay = -1;
    public $skillid = 0;

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'subjs' => 'Результаты ЕГЭ, которые будут предъявлены при подаче заявления:',
            'form' => 'Форма обучения',
            'pay' => 'Вид возмещения затрат',
            'skillid' => 'Уровень подготовки',
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['subjs', 'form', 'pay', 'skillid'], 'safe'],
        ];
    }

    public function search($params)
    {
        $this->load($params);
        return $this->getAreas($this->subjs, $this->form, $this->pay, $this->skillid);
    }

    public function getFormNames()
    {
        return [
            1 => 'Очная',
            2 => 'Заочная',
            3 => 'Очно-заочная'
        ];
    }

    public function getPayNames()
    {
        return [
            -1 => 'Все',
            0 => 'За счет средств федерального бюджета',
            1 => 'С полным возмещением затрат на обучение'
        ];
    }

    public function getSkillNames()
    {
        return [
            62 => 'Бакалавриат',
            65 => 'Специалитет',
            68 => 'Магистратура',
            70 => 'Аспирантура'
        ];
    }

    /**
     * Gets the greeting
     * @return string The greeting to be displayed to the user
     */
    public function getAreas($subjs = '', $form = 1, $pay = -1, $skillid = 0)
    {
        if (!empty($subjs)) {
            $subjs = implode('_', $subjs);
        }
        $url = "https://sapphire.kgsu.ru/vem-www/pk/site/exec?exec=getAreas&form={$form}&pay={$pay}&subjects={$subjs}&skillid={$skillid}";
        //$url = "http://127.0.0.1/vem-www/pk/site/exec?exec=getAreas&form={$form}&pay={$pay}&subjects={$subjs}&skillid={$skillid}";
        $areas = json_decode(file_get_contents($url))->resultData;
        return $areas;
    }

    public function getDetails($areaid = null, $areaitemid = null)
    {
        $areaitem = (!is_null($areaitemid)) ? "&areaitemid={$areaitemid}" : '';
        $url = "https://sapphire.kgsu.ru/vem-www/pk/site/exec?exec=getAreaInfo&areaid={$areaid}{$areaitem}";
        //$url = "http://127.0.0.1/vem-www/pk/site/exec?exec=getAreaInfo&areaid={$areaid}{$areaitem}";
        $areaDetails = json_decode(file_get_contents($url))->resultData;
        return $areaDetails;
    }

    public function getEgeSubjects()
    {
        $url = "https://sapphire.kgsu.ru/vem-www/pk/site/exec?exec=getEgeSubjects";
        //$url = "http://127.0.0.1/vem-www/pk/site/exec?exec=getEgeSubjects";
        $subjects = json_decode(file_get_contents($url))->resultData;
        return $subjects;
    }

    /**
     * @param $params = [
     *           'f' => 1, //форма обучения
     *           'she' => 0, //второе высшее
     *           'e' => 0, //сокращенка
     *           'mobile' => 0 //0 | 1
     *           ];
     * @return bool|string
     */
    public static function getSpecmark($params)
    {

        $client = new Client();
        $response = $client->createRequest()
            ->setMethod('get')
            ->setUrl('https://sapphire.kgsu.ru/vem-www/pk/report/report')
            ->setFormat(Client::FORMAT_RAW_URLENCODED)
            ->setData(ArrayHelper::merge(['rid' => 10], $params))
            ->send();
        if ($response->isOk) {
            return $response->content;
        }

        throw new NotFoundHttpException('Не получилось связаться с сервером');
    }

    /**
     * @param $params = [
     *           'f' => 1, //форма обучения
     *           'forwww' => 1,
     *           'p' => -1, // платность 0 | 1
     *           'skillid' => 0 // скил 0|68|70,
     *           'she' => 0, //второе высшее
     *           'e' => 0, //сокращенка
     *           'sort' => 0 //0 | 1
     *           'mobile' => 0 //0 | 1
     *           ];
     * @return bool|string
     */
    public static function getStat($params)
    {

        $client = new Client();
        $response = $client->createRequest()
            ->setMethod('get')
            ->setUrl('https://sapphire.kgsu.ru/vem-www/pk/report/report')
            ->setFormat(Client::FORMAT_RAW_URLENCODED)
            ->setData(ArrayHelper::merge(['rid' => 2], $params))
            ->send();
        if ($response->isOk) {
            return $response->content;
        }

        throw new NotFoundHttpException('Не получилось связаться с сервером');
    }

    /**
     * @param $params = [
     *           'f' => 1, //форма обучения
     *           'p' => 0, //платность
     *           'rang' => 1,
     *           'withouthvost' => 0,
     *           'prog' => 0,
     *           'type' => 0,
     *           'att' => 0, // аттестат
     *           'she' => 0, //второе высшее
     *           'e' => 0, //сокращенка
     *           'withoutdeadline' => 0
     *           ];
     * @return bool|string
     */
    public static function getZList($params)
    {
        $url = Url::to('https://sapphire.kgsu.ru/vem-www/pk/report/report');

        $client = new Client();
        $response = $client->createRequest()
            ->setMethod('get')
            ->setUrl($url)
            ->setFormat(Client::FORMAT_RAW_URLENCODED)
            ->setData(ArrayHelper::merge(['rid' => 38], $params))
            ->send();
        if ($response->isOk) {
            return $response->content;
        }

        throw new NotFoundHttpException('Не получилось связаться с сервером');
    }

}