<?php

namespace common\models;

use Yii;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "question".
 *
 * @property int $id
 * @property int|null $parent_id
 * @property string|null $name
 * @property string|null $email
 * @property string|null $contact_phone
 * @property string|null $body
 * @property int|null $status
 * @property int|null $created_at
 * @property int|null $updated_at
 * @property int|null $created_by
 * @property int|null $updated_by
 */
class Question extends \yii\db\ActiveRecord
{
    const STATUS_NEW = 0;
    const STATUS_ANSWERED = 1;
    const STATUS_HIDDEN = 2;

    function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                ],
            ],
            'blameable' => [
                'class' => BlameableBehavior::class,
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ]
        ];
    }

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'question';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['parent_id', 'status', 'created_at', 'updated_at', 'created_by', 'updated_by'], 'integer'],
            [['body'], 'string'],
            [['name', 'email', 'contact_phone'], 'string', 'max' => 255],
            [['created_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['created_by' => 'id']],
            [['updated_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['updated_by' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'parent_id' => Yii::t('app', 'Parent ID'),
            'name' => 'Имя',
            'email' => 'Email',
            'contact_phone' => 'Телефон',
            'body' => 'Текст',
            'status' => 'Статус',
            'created_at' => 'Создано в',
            'updated_at' => 'Обновлено в',
            'created_by' => 'Создал',
            'updated_by' => 'Обновил',
        ];
    }

    /**
     * Gets query for [[CreatedBy]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }

    /**
     * Gets query for [[UpdatedBy]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getUpdatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'updated_by']);
    }

    public static function getStatusList()
    {
        return [
            self::STATUS_NEW => 'Новое сообщение',
            self::STATUS_ANSWERED => 'Отвечено',
            self::STATUS_HIDDEN => 'Скрытое сообщение',
        ];
    }

    public static function getStatusName($key)
    {
        $arr = [
            self::STATUS_NEW => 'Новое сообщение',
            self::STATUS_ANSWERED => 'Отвечено',
            self::STATUS_HIDDEN => 'Скрытое сообщение',
        ];
        return $arr[$key];
    }

    public static function getStatusColor($key)
    {
        $arr = [
            self::STATUS_NEW => 'warning',
            self::STATUS_ANSWERED => 'success',
            self::STATUS_HIDDEN => 'default',
        ];
        return $arr[$key];
    }

    public function reply()
    {
        $this->status = self::STATUS_ANSWERED;
        return $this->save();
    }

    /**
     * Gets query for [[Question]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getParent()
    {
        return $this->hasOne(self::className(), ['id' => 'parent_id']);
    }

    /**
     * Gets query for [[Question]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getAnswer()
    {
        return $this->hasOne(self::className(), ['parent_id' => 'id']);
    }
}
