<?php

namespace common\widgets\ContactFormWidget;

use common\models\ContactForm;
use common\widgets\ContactFormWidget\assets\ContactFormWidgetAssets;
use yii\base\Widget;

class ContactFormWidget extends Widget
{
    public $send_to;
    public $subject;
    public $scenario = ContactForm::SCENARIO_CONTACT;

    public function init()
    {
        ContactFormWidgetAssets::register($this->view);
        parent::init(); // TODO: Change the autogenerated stub
    }

    public function run()
    {
        $model = new ContactForm();
        $model->subject = $this->subject;
        $model->scenario = $this->scenario;

        return $this->render($this->scenario, [
            'model' => $model,
            'send_to' => $this->send_to
        ]);
    }

}