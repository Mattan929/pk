<?php

namespace common\widgets\ContactFormWidget\assets;

class ContactFormWidgetAssets extends \yii\web\AssetBundle
{
    public $depends = [
        'frontend\assets\AppAsset'
    ];

    public $js = [
        'js/contact-form.js'
    ];

    public $css = [
    ];

    public function init()
    {
        $this->sourcePath = dirname(__FILE__) . '/../web';

        parent::init();
    }
}