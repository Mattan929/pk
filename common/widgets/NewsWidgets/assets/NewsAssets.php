<?php

namespace common\widgets\NewsWidgets\assets;

class NewsAssets extends \yii\web\AssetBundle
{
    public $depends = [
        'frontend\assets\AppAsset'
    ];

    public $js = [
//        'js/news.js'
    ];

    public $css = [
        'css/news.css'
    ];

    public function init()
    {
        $this->sourcePath = dirname(__FILE__) . '/../web';

        parent::init();
    }
}