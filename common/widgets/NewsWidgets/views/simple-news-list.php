<?php

use yii\widgets\ListView;

/* @var $this \yii\web\View */
/** @var boolean $withoutCategory */
/** @var \common\models\Post[] $models */

foreach ($models as $model){
    echo $this->render('_one-news', ['post' => $model, 'withoutCategory' => $withoutCategory]);
}