<?php

use yii\widgets\ListView;

/* @var $this \yii\web\View */
/* @var $dataProvider \yii\data\ActiveDataProvider */
/** @var boolean $withoutCategory */
/** @var boolean $showMore */


echo ListView::widget([
    'dataProvider' => $dataProvider,
    'itemOptions' => ['class' => 'item'],
    'itemView' => function ($model, $key, $index, $widget) use ($withoutCategory){
        return $this->render('_one-news', ['post' => $model, 'withoutCategory' => $withoutCategory]);

        // or just do some echo
        // return $model->title . ' posted by ' . $model->author;
    },
    'pager' => $showMore ? [
        'class' => \kop\y2sp\ScrollPager::className(),
        'item' => '.item',
        'triggerText' => 'ПОКАЗАТЬ ЕЩЕ',
        'triggerTemplate' => '<div class="wrapper-show-more text-center ias-trigger" style="cursor: pointer"><a>{text}</a></div>',
        'noneLeftText' => 'Записей больше нет',] : []
]);