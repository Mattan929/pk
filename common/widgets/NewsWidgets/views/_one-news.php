<?php

use yii\helpers\Url;

/** @var \common\models\Post $post */
/** @var bool $withoutCategory */

if (!isset($withoutCategory)) $category = $post->category;
$showMore = '... ' . \yii\helpers\Html::a(' Читать дальше &#8594;', ['index/post', 'id' => $post->id]);
$mainImage = $post->post_image;
?>

<div class="one-news">
    <?php
    if (!isset($withoutCategory)) { ?>
        <div class="news-category" style="background-color: <?= !empty($category) ? $category->post_excerpt : '' ?>">
            <a href="<?= Url::toRoute(['index/news', 'newscat' => $category->id]) ?>">
                <?= !empty($category) ? $category->title : 'Без категории' ?>
            </a>
        </div>
    <?php }
    ?>
    <div class="col-sm-12 col-md-4 col-lg-4 img-block">
        <a href="<?= Url::toRoute(['site/post', 'id' => $post->id]) ?>">
            <img src="<?= $mainImage !== null ? $mainImage->href : ''?>"
                 alt="<?= $post->title ?>">
        </a>
    </div>
    <div class="col-sm-12 col-md-8 col-lg-8 text-block">
        <a href="<?= Url::toRoute(['site/post', 'id' => $post->id]) ?>">
            <h4 class="postdesc"><?= strip_tags($post->title) ?></h4>
        </a>
        <p style="margin-bottom: 20px">
            <?= $post->description ?>
        </p>
    </div>
    <br>
    <div class="col-md-offset-4 col-sm-offset-12 col-lg-offset-4 text-muted date-block">
        <?= Yii::$app->formatter->asDate($post->publish_date, 'dd MMMM Y hh:mm') ?>
    </div>
</div>
