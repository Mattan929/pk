<?php
return [
    'name' => 'Приемная комиссия КГУ',
    'aliases' => [
        '@bower' => '@vendor/bower-asset',
        '@npm'   => '@vendor/npm-asset',
    ],
    'language' => 'ru_RU',
    'bootstrap' => [
        'dektrium\user\Bootstrap',
        'dektrium\rbac\Bootstrap',
    ],
    'vendorPath' => dirname(dirname(__DIR__)) . '/vendor',
    'extensions' => yii\helpers\ArrayHelper::merge(
        require( dirname(dirname(__DIR__)) . '/vendor/yiisoft/extensions.php'),
        [
            'dektrium/yii2-rbac' => [
                'name' => 'dektrium/yii2-rbac',
                'version' => '1.0.1',
                'alias' => [
                    '@dektrium/rbac' => '@dektrium/rbac',
                ],
            ],
            'dektrium/yii2-user' => [
                'name' => 'dektrium/yii2-user',
                'version' => '0.9.14.0',
                'alias' => [
                    '@dektrium/user' => '@dektrium/user',
                ],
                'bootstrap' => 'dektrium\\user\\Bootstrap',
            ],
        ]
    ),
    'components' => [
        'soapClientManager' => [
            'class' => 'common\components\soap\soapClientManager',
            'wsdl' => 'http://1c.kgsu.ru/copyuni02042021/ws/webabit.1cws?wsdl',
            'login' =>  'WebAbit',
            'password' => '29Dsc<f,GhjPvt',
            'debug' => 1
        ],
        'devicedetect' => [
            'class' => alexandernst\devicedetect\DeviceDetect::class
        ],
        'assetManager' => [
            'appendTimestamp' => true,
//            'forceCopy' => YII_DEBUG
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
    ],
    'modules' => [
        'menu' => [
            'class' => '\pceuropa\menu\Menu',
        ],
        'event-calendar' => [
            'class' => 'mattan929\calendar\Module',
        ],
        'files' => [
            'class' => 'floor12\files\Module',
            'storage' => '@frontend/web/images/storage',
            'cache' => '@frontend/web/images/storage_cache',
            'token_salt' => 'pk_kgsu_2021_01_random_salt',
        ],
        'user' => [
            'class' => 'dektrium\user\Module',
            'enableConfirmation' => false,
            'enableGeneratingPassword' => false,
            'admins' => ['administrator'],
            'adminPermission' => 'superadmin'
        ],
        'rbac' => [
            'class' => 'dektrium\rbac\RbacWebModule',
            'adminPermission' => 'superadmin'
        ],
    ],
];
