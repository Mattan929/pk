<?php
return [
    'adminEmail' => 'admin@example.com',
    'supportEmail' => 'support@example.com',
    'senderEmail' => 'noreply@kgsu.ru',
    'senderName' => 'Приемная комиссия КГУ',
    'user.passwordResetTokenExpire' => 3600,
];
