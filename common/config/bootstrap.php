<?php
Yii::setAlias('@common', dirname(__DIR__));
Yii::setAlias('@frontend', dirname(dirname(__DIR__)) . '/frontend');
Yii::setAlias('@backend', dirname(dirname(__DIR__)) . '/backend');
Yii::setAlias('@console', dirname(dirname(__DIR__)) . '/console');
Yii::setAlias('@dektrium/user', dirname(dirname(__DIR__)) . '/common/modules/dektrium/yii2-user');
Yii::setAlias('@dektrium/rbac', dirname(dirname(__DIR__)) . '/common/modules/dektrium/yii2-rbac');
Yii::setAlias('@mattan929/calendar', dirname(dirname(__DIR__)) . '/common/modules/mattan929/yii2-module-event-calendar/src');
Yii::setAlias('@pceuropa/menu', dirname(dirname(__DIR__)) . '/common/modules/pceuropa/yii2-menu');
Yii::setAlias('@alexandernst/devicedetect', dirname(dirname(__DIR__)) . '/common/components/alexandernst/devicedetect');
Yii::setAlias('@Detection', dirname(dirname(__DIR__)) . '/common/components/detection/namespaced/Detection');